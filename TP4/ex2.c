#include <stdio.h>
#include <math.h>
#include <fcntl.h>
#include "../lib/h/image_ppm.h"
#include "../lib/h/tp.h"

void toGreyscale(OCTET*, OCTET*, int,int);

int main(int argc, char* argv[]) {
	if (argc != 3) {
		printf("Usage : \n\ttoGreyscale ImgIn.ppm ImgOut.pgm\n\n");
		exit(1);
	} else if (strcmp((const char*)"--usage", (const char*)argv[1]) == 0) {
		printf("Usage : \n\ttoGreyscale ImgIn.ppm ImgOut.pgm\n\n");
		exit(1);
	}

	// Arguments nécessaires au programme
	char nomFichierEntree[255];
	char nomFichierSortie[255];
	int imgHeight;
	int imgWidth;
	int imgSize;

	// Prise des noms de fichier pour 
	// la lecture et l'ecriture
	sscanf (argv[1],"%s",nomFichierEntree);
	sscanf (argv[2],"%s",nomFichierSortie);

	OCTET *ImgIn;
	OCTET *ImgOut;

	// Prendre taille image
	lire_nb_lignes_colonnes_image_ppm(nomFichierEntree, &imgHeight, &imgWidth);
	imgSize = imgHeight * imgWidth;

	// Lire fichiers donné en argument
	allocation_tableau(ImgIn, OCTET, imgSize*3);
	lire_image_ppm(nomFichierEntree, ImgIn, imgSize);
	allocation_tableau(ImgOut, OCTET, imgSize);

	// Rendre l'image en grayscale;
	toGreyscale(ImgIn, ImgOut, imgWidth, imgHeight);

	ecrire_image_pgm(nomFichierSortie, ImgOut, imgHeight, imgWidth);

	int* histo = histogramme(ImgOut, imgWidth, imgHeight, "plot/ex2_histo.dat");

	free(ImgIn);
	free(ImgOut);
	free(histo);

	return 0;
}


void toGreyscale(OCTET* in, OCTET* out, int imgWidth, int imgHeight) {
	// Ici, on part du principe que le capteur utilisé
	// est un capteur CMOS : contient donc un filtre de Bayer
	// qui a normalement été dématricé (de-Bayered) et
	// par la suite convertie en JPEG. Coefficients pour la
	// luminance depuis RGB pris sur Wikipedia
	// Donc les coefficients pour les couleurs sont :
	// RED   : 0.2126
	// GREEN : 0.7152
	// BLUE  : 0.0722

	double greyValue = 0.0;

	// On calcule la valeur de gris avec les coefs donnés au début de la fonction
	for (int i = 0; i < imgHeight; ++i) {
		for (int j = 0; j < imgWidth*3; j += 3) {
			greyValue += (double)0.2126 * (double)in[i*imgWidth*3+j+0];	// Red
			greyValue += (double)0.7152 * (double)in[i*imgWidth*3+j+1];	// Green
			greyValue += (double)0.0722 * (double)in[i*imgWidth*3+j+2];	// Blue
			// printf("grey : %i\n", (int)greyValue);
			out[i*imgWidth + (j/3)] = (OCTET)floor(greyValue);
			greyValue = 0.0;
		}
	}
}
