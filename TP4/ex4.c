#include <stdio.h>
#include <math.h>
#include <fcntl.h>
#include "../lib/h/image_ppm.h"
#include "../lib/h/tp.h"

void applyBlurKernel(OCTET*, OCTET*, int, int, double**, int, int);

int main(int argc, char* argv[]) {
	if (argc != 3) {
		printf("Usage : \n\tflouGeneral ImgIn.ppm ImgOut.ppm\n\n");
		exit(1);
	} else if (strcmp((const char*)"--usage", (const char*)argv[1]) == 0) {
		printf("Usage : \n\tflouGeneral ImgIn.ppm ImgOut.ppm\n\n");
		exit(1);
	}

	// Arguments nécessaires au programme
	char nomFichierEntree[255];
	char nomFichierSortie[255];
	int imgHeight;
	int imgWidth;
	int imgSize;

	// Prise des noms de fichier pour 
	// la lecture et l'ecriture
	sscanf (argv[1],"%s",nomFichierEntree);
	sscanf (argv[2],"%s",nomFichierSortie);

	OCTET *ImgIn;
	OCTET *ImgOut;

	// Prendre taille image
	lire_nb_lignes_colonnes_image_ppm(nomFichierEntree, &imgHeight, &imgWidth);
	imgSize = imgHeight * imgWidth;

	// Lire fichiers donné en argument
	allocation_tableau(ImgIn, OCTET, imgSize*3);
	lire_image_ppm(nomFichierEntree, ImgIn, imgSize);
	allocation_tableau(ImgOut, OCTET, imgSize*3);
	int kernelSize = 15;

	// Creer kernel :
	double** kernel = calloc(kernelSize, sizeof(double*));
	for (int i = 0; i < kernelSize; ++i) {
		kernel[i] = calloc(kernelSize, sizeof(double));
	}

	double coef = 1.0;

	for (int i = 0; i < kernelSize; ++i) {
		for (int j = 0; j < kernelSize; ++j) {
			kernel[i][j] = coef * (1.0 / (double)( kernelSize * kernelSize ));
		}
	}

	// Flouter le fond.
	applyBlurKernel(ImgIn, ImgOut, imgWidth, imgHeight, kernel, kernelSize, WHITE);

	ecrire_image_ppm(nomFichierSortie, ImgOut, imgHeight, imgWidth);

	// free(histoValues);
	free(ImgIn);
	free(ImgOut);

	return 0;
}

void applyBlurKernel(OCTET* in, OCTET* out, int imgWidth, int imgHeight, double** kernel, int kernelSize, int target) {
	// Prendre le centre du kernel :
	int kernelCenter = (kernelSize % 2 == 1) ? (kernelSize + 1)/2 - 1 : kernelSize/2 - 1;

	// Parcours de l'image :
	for (int i = 0; i < imgHeight; ++i) {
		for (int j = 0; j < imgWidth*3; j += 3) {
			// On va appliquer le kernel aux pixels
			// environnants du pixel analysé :
			
			int xOffset, yOffset;

			double newValueR = 0.0;
			double newValueG = 0.0;
			double newValueB = 0.0;

			double coefToDivideWith = 0.0;

			// Parcours depuis le centre du kernel jusqu'a ses extrémités
			//
			// X varie de haut en bas, Y de gauche à droite
			//
			// Pour tous les X de chaque cotés du centre kernel
			for (int x = kernelCenter; x >= 0; --x) {
				// Calculer la distance kernel : (toujours positive)
				xOffset = kernelCenter - x;
				// Si on dépasse pas le bord gauche de l'image
				if ((j)-xOffset*3 >= 0) {
					// Pour tous les Y de chaque coté du centre kernel
					for (int y = kernelCenter; y >= 0; --y) {
						// Calculer la distance du centre kernel
						yOffset = kernelCenter - y;
						// Si on dépasse le haut de l'image, et que le kernel est censé masquer :
						if (i-yOffset >= 0) {
							newValueR += kernel[kernelCenter - yOffset][kernelCenter - xOffset] * (double)in[(i-yOffset)*imgWidth*3 + j-xOffset*3 + 0];
							newValueG += kernel[kernelCenter - yOffset][kernelCenter - xOffset] * (double)in[(i-yOffset)*imgWidth*3 + j-xOffset*3 + 1];
							newValueB += kernel[kernelCenter - yOffset][kernelCenter - xOffset] * (double)in[(i-yOffset)*imgWidth*3 + j-xOffset*3 + 2];
							coefToDivideWith += kernel[kernelCenter - yOffset][kernelCenter - xOffset];
						}
						// Sinon, si on dépasse pas le bas de l'image, et que le kernel masque :
						if (i+yOffset <= imgHeight) {
							newValueR += kernel[kernelCenter + yOffset][kernelCenter - xOffset] * (double)in[(i-yOffset)*imgWidth*3 + j-xOffset*3 + 0];
							newValueG += kernel[kernelCenter + yOffset][kernelCenter - xOffset] * (double)in[(i-yOffset)*imgWidth*3 + j-xOffset*3 + 1];
							newValueB += kernel[kernelCenter + yOffset][kernelCenter - xOffset] * (double)in[(i-yOffset)*imgWidth*3 + j-xOffset*3 + 2];
							coefToDivideWith += kernel[kernelCenter + yOffset][kernelCenter - xOffset];
						}
					}
				} 
				// Si on ne dépasse pas le bord droit de l'image
				if ((j)+xOffset*3 <= imgWidth) {
					// Pour tous les Y de chaque côté du centre kernel :
					for (int y = kernelCenter; y >= 0; --y) {
						// Calculer la distance du centre kernel :
						yOffset = kernelCenter - y;
						// Si on ne dépasse pas le haut de l'image, et que le kernel est censé masquer :
						if (i-yOffset >= 0) { 
							newValueR += kernel[kernelCenter - yOffset][kernelCenter + xOffset] * (double)in[(i-yOffset)*imgWidth*3 + j+xOffset*3 + 0];
							newValueG += kernel[kernelCenter - yOffset][kernelCenter + xOffset] * (double)in[(i-yOffset)*imgWidth*3 + j+xOffset*3 + 1];
							newValueB += kernel[kernelCenter - yOffset][kernelCenter + xOffset] * (double)in[(i-yOffset)*imgWidth*3 + j+xOffset*3 + 2];
							coefToDivideWith += kernel[kernelCenter - yOffset][kernelCenter + xOffset];
						}
						// Sinon, si on dépasse pas le bas de l'image, et que le kernel masque :
						if (i+yOffset <= imgHeight) {
							newValueR += kernel[kernelCenter + yOffset][kernelCenter + xOffset] * (double) in[(i+yOffset)*imgWidth*3 + j+xOffset*3 + 0];
							newValueG += kernel[kernelCenter + yOffset][kernelCenter + xOffset] * (double) in[(i+yOffset)*imgWidth*3 + j+xOffset*3 + 1];
							newValueB += kernel[kernelCenter + yOffset][kernelCenter + xOffset] * (double) in[(i+yOffset)*imgWidth*3 + j+xOffset*3 + 2];
							coefToDivideWith += kernel[kernelCenter + yOffset][kernelCenter + xOffset];
						}
					}
				}
			}
			
			// Applique la nouvelle valeur au pixel :
			out[i*imgWidth*3 + j + 0] = (OCTET)round( newValueR / coefToDivideWith );
			out[i*imgWidth*3 + j + 1] = (OCTET)round( newValueG / coefToDivideWith );
			out[i*imgWidth*3 + j + 2] = (OCTET)round( newValueB / coefToDivideWith );
		}
	}
}
