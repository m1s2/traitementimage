#include <stdio.h>
#include <math.h>
#include <fcntl.h>
#include "../lib/h/image_ppm.h"
#include "../lib/h/tp.h"

enum typeDeRepliement {
	BOTTOM,TOP
};

enum colorOffsets {
	RED,
	GREEN,
	BLUE
};

void toGreyscale(OCTET*, OCTET*, int,int);
void applyBlurKernel(OCTET*, OCTET*, OCTET*, int, int, double**, int, int);

int main(int argc, char* argv[]) {
	if (argc != 4) {
		printf("Usage : \n\tflouFond ImgIn.ppm ImgMask.pgm ImgOut.pgm\n\n");
		exit(1);
	} else if (strcmp((const char*)"--usage", (const char*)argv[1]) == 0) {
		printf("Usage : \n\tflouFond ImgIn.ppm ImgMask.pgm ImgOut.pgm\n\n");
		exit(1);
	}

	// Arguments nécessaires au programme
	char nomFichierEntree[255];
	char nomFichierMasque[255];
	char nomFichierSortie[255];
	int imgHeight;
	int imgWidth;
	int imgSize;

	// Prise des noms de fichier pour 
	// la lecture et l'ecriture
	sscanf (argv[1],"%s",nomFichierEntree);
	sscanf (argv[2],"%s",nomFichierMasque);
	sscanf (argv[3],"%s",nomFichierSortie);

	OCTET *ImgIn;
	OCTET* ImgSeuil;
	OCTET *ImgOut;

	// Prendre taille image
	lire_nb_lignes_colonnes_image_ppm(nomFichierEntree, &imgHeight, &imgWidth);
	imgSize = imgHeight * imgWidth;

	// Lire fichiers donné en argument
	allocation_tableau(ImgIn, OCTET, imgSize*3);
	lire_image_ppm(nomFichierEntree, ImgIn, imgSize);
	allocation_tableau(ImgSeuil, OCTET, imgSize);
	lire_image_pgm(nomFichierMasque, ImgSeuil, imgSize);
	allocation_tableau(ImgOut, OCTET, imgSize*3);
	int kernelSize = 15;

	double** kernel = calloc(kernelSize, sizeof(double*));
	for (int i = 0; i < kernelSize; ++i) {
		kernel[i] = calloc(kernelSize, sizeof(double));
	}

	double coef = 1.0;

	for (int i = 0; i < kernelSize; ++i) {
		for (int j = 0; j < kernelSize; ++j) {
			kernel[i][j] = coef * (1.0 / (double)( kernelSize * kernelSize ));
		}
	}

	// Flouter le fond.
	applyBlurKernel(ImgIn, ImgSeuil, ImgOut, imgWidth, imgHeight, kernel, kernelSize, WHITE);

	ecrire_image_ppm(nomFichierSortie, ImgOut, imgHeight, imgWidth);

	// free(histoValues);
	free(ImgIn);
	free(ImgOut);
	free(ImgSeuil);

	return 0;
}


void toGreyscale(OCTET* in, OCTET* out, int imgWidth, int imgHeight) {
	// Ici, on part du principe que le capteur utilisé
	// est un capteur CMOS : contient donc un filtre de Bayer
	// qui a normalement été dématricé (de-Bayered) et
	// par la suite convertie en JPEG. Coefficients pour la
	// luminance depuis RGB pris sur Wikipedia
	// Donc les coefficients pour les couleurs sont :
	// RED   : 0.2126
	// GREEN : 0.7152
	// BLUE  : 0.0722

	double greyValue = 0.0;

	// On calcule la valeur de gris avec les coefs donnés au début de la fonction
	for (int i = 0; i < imgHeight; ++i) {
		for (int j = 0; j < imgWidth*3; j += 3) {
			greyValue += (double)0.2126 * (double)in[i*imgWidth*3+j+0];	// Red
			greyValue += (double)0.7152 * (double)in[i*imgWidth*3+j+1];	// Green
			greyValue += (double)0.0722 * (double)in[i*imgWidth*3+j+2];	// Blue
			// printf("grey : %i\n", (int)greyValue);
			out[i*imgWidth + (j/3)] = (OCTET)floor(greyValue);
			greyValue = 0.0;
		}
	}
}

void applyBlurKernel(OCTET* in, OCTET* mask, OCTET* out, int imgWidth, int imgHeight, double** kernel, int kernelSize, int target) {
	// Prendre le centre du kernel :
	int kernelCenter = (kernelSize % 2 == 1) ? (kernelSize + 1)/2 - 1 : kernelSize/2 - 1;

	// Parcours de l'image :
	for (int i = 0; i < imgHeight; ++i) {
		for (int j = 0; j < imgWidth*3; j += 3) {
			// On va appliquer le kernel aux pixels
			// environnants du pixel analysé :
			
			int xOffset, yOffset;

			double newValueR = 0.0;
			double newValueG = 0.0;
			double newValueB = 0.0;

			double coefToDivideWith = 0.0;

			int maskingIndex = i * imgWidth + j/3;

			// Parcours depuis le centre du kernel jusqu'a ses extrémités
			//
			// X varie de haut en bas, Y de gauche à droite
			//
			// Pour tous les X de chaque cotés du centre kernel
			for (int x = kernelCenter; x >= 0; --x) {
				// Calculer la distance kernel : (toujours positive)
				xOffset = kernelCenter - x;
				// Si on dépasse pas le bord gauche de l'image
				if ((j)-xOffset*3 >= 0) {
					// Pour tous les Y de chaque coté du centre kernel
					for (int y = kernelCenter; y >= 0; --y) {
						// Calculer la distance du centre kernel
						yOffset = kernelCenter - y;
						// Si on dépasse le haut de l'image, et que le kernel est censé masquer :
						if (i-yOffset >= 0 && mask[maskingIndex] == target) { //
							newValueR += kernel[kernelCenter - yOffset][kernelCenter - xOffset] * (double)in[(i-yOffset)*imgWidth*3 + j-xOffset*3 + 0];
							newValueG += kernel[kernelCenter - yOffset][kernelCenter - xOffset] * (double)in[(i-yOffset)*imgWidth*3 + j-xOffset*3 + 1];
							newValueB += kernel[kernelCenter - yOffset][kernelCenter - xOffset] * (double)in[(i-yOffset)*imgWidth*3 + j-xOffset*3 + 2];
							coefToDivideWith += kernel[kernelCenter - yOffset][kernelCenter - xOffset];
						}
						// Sinon, si on dépasse pas le bas de l'image, et que le kernel masque :
						if (i+yOffset <= imgHeight && mask[maskingIndex] == target) { //
							newValueR += kernel[kernelCenter + yOffset][kernelCenter - xOffset] * (double)in[(i-yOffset)*imgWidth*3 + j-xOffset*3 + 0];
							newValueG += kernel[kernelCenter + yOffset][kernelCenter - xOffset] * (double)in[(i-yOffset)*imgWidth*3 + j-xOffset*3 + 1];
							newValueB += kernel[kernelCenter + yOffset][kernelCenter - xOffset] * (double)in[(i-yOffset)*imgWidth*3 + j-xOffset*3 + 2];
							coefToDivideWith += kernel[kernelCenter + yOffset][kernelCenter - xOffset];
						}
					}
				} 
				// Si on ne dépasse pas le bord droit de l'image
				if ((j)+xOffset*3 <= imgWidth) {
					// Pour tous les Y de chaque côté du centre kernel :
					for (int y = kernelCenter; y >= 0; --y) {
						// Calculer la distance du centre kernel :
						yOffset = kernelCenter - y;
						// Si on ne dépasse pas le haut de l'image, et que le kernel est censé masquer :
						if (i-yOffset >= 0 && mask[maskingIndex] == target) { // 
							newValueR += kernel[kernelCenter - yOffset][kernelCenter + xOffset] * (double)in[(i-yOffset)*imgWidth*3 + j+xOffset*3 + 0];
							newValueG += kernel[kernelCenter - yOffset][kernelCenter + xOffset] * (double)in[(i-yOffset)*imgWidth*3 + j+xOffset*3 + 1];
							newValueB += kernel[kernelCenter - yOffset][kernelCenter + xOffset] * (double)in[(i-yOffset)*imgWidth*3 + j+xOffset*3 + 2];
							coefToDivideWith += kernel[kernelCenter - yOffset][kernelCenter + xOffset];
						}
						// Sinon, si on dépasse pas le bas de l'image, et que le kernel masque :
						if (i+yOffset <= imgHeight && mask[maskingIndex] == target) { //
							newValueR += kernel[kernelCenter + yOffset][kernelCenter + xOffset] * (double) in[(i+yOffset)*imgWidth*3 + j+xOffset*3 + 0];
							newValueG += kernel[kernelCenter + yOffset][kernelCenter + xOffset] * (double) in[(i+yOffset)*imgWidth*3 + j+xOffset*3 + 1];
							newValueB += kernel[kernelCenter + yOffset][kernelCenter + xOffset] * (double) in[(i+yOffset)*imgWidth*3 + j+xOffset*3 + 2];
							coefToDivideWith += kernel[kernelCenter + yOffset][kernelCenter + xOffset];
						}
					}
				}
			}
			if (coefToDivideWith != 0.0) {
			// Applique la nouvelle valeur au pixel :
				out[i*imgWidth*3 + j + 0] = (OCTET)round( newValueR / coefToDivideWith );
				out[i*imgWidth*3 + j + 1] = (OCTET)round( newValueG / coefToDivideWith );
				out[i*imgWidth*3 + j + 2] = (OCTET)round( newValueB / coefToDivideWith );
			} else {
				out[i*imgWidth*3 + j + 0] = in[i*imgWidth*3 + j + 0];
				out[i*imgWidth*3 + j + 1] = in[i*imgWidth*3 + j + 1];
				out[i*imgWidth*3 + j + 2] = in[i*imgWidth*3 + j + 2];
			}
		}
	}
}