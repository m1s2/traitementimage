#include <stdio.h>
#include <math.h>
#include <fcntl.h>
#include "../lib/h/image_ppm.h"
#include "../lib/h/tp.h"

void roc(OCTET*, OCTET*, int, int*, int*, int*, int*, int*, int*, int);

int main(int argc, char* argv[]) {
	if (argc != 3) {
		printf("Usage : \n\troc ImgIn.pgm ImgSeuil.pgm\n\n");
		exit(1);
	} else if (strcmp((const char*)"--usage", (const char*)argv[1]) == 0) {
		printf("Usage : \n\troc ImgRef.pgm ImgSeuil.pgm\n\n");
		exit(1);
	}

	// Arguments nécessaires au programme
	char nomFichierEntree[255];
	char nomFichierSortie[255];
	int imgHeight;
	int imgWidth;
	int imgSize;

	// Prise des noms de fichier pour 
	// la lecture et l'ecriture
	sscanf (argv[1],"%s",nomFichierEntree);
	sscanf (argv[2],"%s",nomFichierSortie);

	OCTET *ImgIn;
	OCTET *ImgOut;
	OCTET* ImgBase;

	// Prendre taille image
	lire_nb_lignes_colonnes_image_pgm(nomFichierEntree, &imgHeight, &imgWidth);
	imgSize = imgHeight * imgWidth;

	// Lire fichiers donné en argument
	allocation_tableau(ImgIn, OCTET, imgSize);
	lire_image_pgm(nomFichierEntree, ImgIn, imgSize);
	allocation_tableau(ImgBase, OCTET, imgSize);
	lire_image_pgm(nomFichierSortie, ImgBase, imgSize);
	allocation_tableau(ImgOut, OCTET, imgSize);

	FILE* rocCurve = fopen("plot/roc.dat","w");
	FILE* rocData = fopen("plot/roc_raw.dat","w");

	int tp = 0;
	int fp = 0;
	int tn = 0;
	int fn = 0;
	int tError = __INT_MAX__;
	int serror = 256;

	for (int i = 0; i < 256; ++i) {
		seuillage(ImgBase, ImgOut, imgWidth, imgHeight, i);
		roc(ImgIn, ImgOut, imgSize, &tp, &tn, &fp, &fn, &tError, &serror, i);
		double sensi = (double)tp / ( (double)tp + (double)fn );
		double speci = 1.0 - (double)tn / ( (double)tn + (double)fp );
		fprintf(rocData, "%i %i %i %i %i %f %f\n", i, tp, tn, fp, fn, speci, sensi);
		fprintf(rocCurve, "%f %f\n", speci, sensi);
	}

	printf("Taux erreur minimal : %i au seuil %i\n", tError, serror);

	fclose(rocCurve);
	fclose(rocData);

	free(ImgIn);
	free(ImgOut);

	return 0;
}

void roc(OCTET* ImgRef, OCTET* ImgSeuil, int imgSize, int* truePos, int* trueNeg, int* falsePos, int* falseNeg, int* Terror, int* SerrorMin, int seuil) {
	*truePos = 0;
	*trueNeg = 0;
	*falseNeg = 0;
	*falsePos = 0;
	for (int i = 0; i < imgSize; ++i) {
		if (ImgRef[i] == WHITE && ImgSeuil[i] == WHITE) {
			// Negatif negatif
			(*trueNeg)++;
		} else if (ImgRef[i] == BLACK && ImgSeuil[i] == WHITE) {
			// Positif Negatif
			(*falseNeg)++;
		} else if (ImgRef[i] == BLACK && ImgSeuil[i] == BLACK) {
			(*truePos)++;
		} else if (ImgRef[i] == WHITE && ImgSeuil[i] == BLACK) {
			(*falsePos)++;
		}
	}
	int cTerror = (*falseNeg) + (*falsePos);
	if (cTerror < (*Terror)) {
		(*SerrorMin) = seuil;
		(*Terror) = cTerror;
	}
}