# Sujet 1 : Merge and Split

### Cédric Pluta

- 2 phases
- Demande un critère d'homogénéité
- Composé de quad tree
- Subdivise les régions en 4, puis regarde le critère d'homogénéité, et redivise ou fusionne selon ce critère.
- Si les critères pour le merge sont trop proches, alors on risque de contouring autour des objets. Au contaire s'ils sont trop séparés, alors on risque de trop lisser l'objet
- Fait en plusieurs passes
- Utilisé pour imag.médic., pour séparer les organes
- Extension aux couleurs : passage en HSV

# Elliot et Baptiste

- Sinon matrices d'adjacences, pour déterminer les voisins
- Merge semble compliqué
- Extension aux couleurs : Compliquée, par composante semble promettrice mais le merge des plans est non défini

# Inpainting

- Patch Match : pas mal, faudrait voir

# Mosaïque

### Aloïs Marrocq & Léo Veyssière

- calcul de toutes les variances/moyennes des imagettes
- découpe en carreaux d'images
- calcul de moyenne variance chaque carreau
- comparaison avec moyennes du fichier .dat
- prends l'image qui a la plus petite variance parmi toutes celles qui ont la moyenne a +- 5 du carreau
- image par variance laisse quelques petits points blancs au milieu

### Raphaël Courbier & Homère Bourgeois

- Génération d'images plus grandes, on peut zoomer pour voir les imagettes
- MINIMISER TOUTES LES CHOSES
- Fourier en extension pour éviter d'avoir des images dont la variance peut être égale mais la couleur deminante peut différer

### Quentin et Pierre

- Découpage de l'image en blocs de taille égale
- Transition qu fondu qui passe crème sa mère
- Divergeance de Kullback-Leibler (?)
- Macron il est beau avec ses imagettes. Sans, pas tellement.
- Les niveaux de gris par quadrans semblent être les plus prometteurs
- Puech aime les calculs de dimensionnement <3

# Reconnaissance faciale par EigenFaces

### Corentin Gauthier & Emmanuel Louchez

- Nécessite une BDD de visages
- Permet de savoir si un visage A est similaire à un visage B
- Étapes :
    - Calcul du visage moyen
    - Calcul des caractéristiques propres aux visages
    - Application PCA
- Moyenne "bête" sur tous pixels de toutes images pour le visage moyen
- Caractéristiques définies grâce à une matrice de covariance
- Application de PCA aux caractéristiques pour ne garder que les infos pertinentes

### Salim Ben-Dakhlia & Bianca Jensen Van Rensburg

- Utilisé dans le domaine de la biométrie
- Utilisation de PCA ici aussi
- Images normalisées avant de les traiter
- Calcul : image d'un visage - visage moyen --> (projeté sur) l'eigenSpace calculé auparavant // ensuite calcul de distance avec les 

# Bibliothèque d'images

### Guillaume Fabre (& Stefan Hartinger)

- Bibliothèque de traitement d'images dévelopée pour besoins personnels
- Passage en c++ uniquement
- Ajout d'une interface graphique, conversion et cleanup du code fait en partie
- Interface graphique en Qt
- Fonctionnalités de base : binarisation, ajout de flou gaussien, de bruit gaussien, égalisation, délimitation de bord (érosion+dilatation), chargement et sauvegarde d'images

# Détection de points caractéristiques

### Nicolas Calvet & Thibault Odorico

- Les coins sont les points les plus importants dans une image
- FAST utilise du ML pour déterminer les points qui sont des coins par rapport à leur voisinage
- ORB utilise FAST+BRIEF pour mieux détecter les coins grace au descripteur BRIEF
- SIFT, assez lent mais très robuste au changement d'échelle, et de rotation
- SURF, comme SIFT, but faster.
- SURF choisi pour après
- Si le changement de PdV entre deux images est trop important, il se peut que SURF ne trouve aucune bonne correspondance

### Maëlle Beuret & Julien Lesinski

- Explication en profondeur de SIFT/SURF, trop compliquée pour ici
- Floutage du SURF accentue les erreurs possibles sur l'image non floutée

# Effets spéciaux

### Mirai Skendraoui et Teiki Raihauti

- Effets : Rotation, pixellisation, sphère(fisheye), spirale, vague
- Rotation : chaque pixel est rotaté par rapport au centre de l'image, arrondi au plus proche
- Pixellisation : découpage en blocs de nxn carrés, chaque bloc est donné la moyenne des pixels les composant
- Sphere(fisheye) : sur chacun des pixels on applique une transformation fisheye
- Spirale : chaque pixel tourné selon un angle qui dépend de la distance par rapport au centre
- vague{lette} : assez complexe mathématiquement
- time warp : {genre fisheye inversé} 


### Les deux Thomas

- Normalisation des couleurs
- Inversion des couleurs
- Filtre sépia
- Ajout de bruit : nécessite la moyenne et la variance de l'image
- Déformation vaguelette/goutelette
- Dessin au crayon : inversion image + blur image inversée + blend image inversée avec originelle (blend : color dodge)
- Cartoon : blur + bland avec original (blend : darken only)
- Peinture à l'huile
- Faceswap abordé via ML et seamless cloning

# Analyse Confocale

- But : détecter le premier plan, l'extraire de l'image, flouter le fond, et remettre le premier plan par dessus
- Peut aussi servir pour "coller" plusieurs images ensembles qui ont des niveaux de flou différents
