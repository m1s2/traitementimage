#ifndef TP_H_INCLUDED
#define TP_H_INCLUDED

#include "image_ppm.h"

// Types de "cases" kernel :
enum kernelTypes {
	TRANSPARENT,
	OPAQUE
};

// Types de target de l'algo d'application kernel :
enum targetTypes {
	BLACK = 0,
	WHITE = 255
};

/**
 * [Source : TP1, ex1] Permet de seuiller une image donnée sous format OCTET* de taille  
 * imgWidth * imgHeight selon la valeur de nuance de gris nommée threshold, et à stocker 
 * le résultat dans ImgOut. Les images données doivent être au format PGM afin que l'on  
 * puisse effectuer le traitement comme on le souhaite.
 * 
 * @param ImgIn image source
 * @param ImgOut image de destination
 * @param imgWidth largeur de l'image
 * @param imgHeight hauteur de l'image
 * @param threshold valeur de seuil en nuance de gris (compris entre 0 et 255)
 */
void seuillage(OCTET* ImgIn, OCTET* ImgOut, int imgWidth, int imgHeight, int threshold);

/**
 * [Source : TP1, ex2] Permet de seuiller une image donnée sous format OCTET* de taille  
 * imgWidth * imgHeight selon les 'quantityThresholds' valeurs de nuance de gris stockées dans *thresholds, et à stocker 
 * le résultat dans ImgOut. Les images données doivent être au format PGM afin que l'on  
 * puisse effectuer le traitement comme on le souhaite. 
 * 
 * Attention : ne supporte que 3 ou 4 valeurs de seuillage.
 * 
 * @param ImgIn image source
 * @param ImgOut image de destination
 * @param imgWidth largeur de l'image
 * @param imgHeight hauteur de l'image
 * @param thresholds valeur de seuil en nuance de gris (compris entre 0 et 255)
 * @param quantityThresholds nombre de valeurs de seuil contenues dans le tableau
 */
void seuillageMultiple(OCTET* ImgIn, OCTET* ImgOut, int imgWidth, int imgHeight, int* thresholds, int quantityThresholds);

/**
 * [Source : TP1 ex3] Permet d'obtenir le profil d'une image selon une composante donnée.
 * On peut ainsi demander le profil d'une ligne, ou d'une colonne, parmi les n (respectivement
 * m) lignes (repectivement colonnes) de l'image ImgIn donnée en paramètre. Les résultats seront stockés dans 
 * une fichier externe, en format non traité (chaque ligne contient la valeur de nuance de gris
 * du pixel), et de manière à pouvoir le visualiser avec un utilitaire tel que GNUPLOT, ou tout autre utilitaire
 * de votre choix supportant les fichiers .dat. Le nom du fichier donné peut être affixé d'une extension, au quel
 * cas l'extension .dat sera rajoutée par la suite, sauf si celle ci est déjà présente.
 * 
 * @param ImgIn image en entrée au format PGM
 * @param imgWidth largeur de l'image donnée
 * @param imgHeight hauteur de l'image donnée
 * @param type le type de profil souhaité. Peut être 'l' pour une ligne (respectivement 'c' pour une colonne)
 * @param indexRequested le numéro de ligne/colonne demandée pour effectuer un profil
 * @param pathToFile une chaîne de caractères représentant le fichier où l'on va écrire le profil d'une ligne/colonne
 */
void profilComposanteImage(OCTET* ImgIn, int imgWidth, int imgHeight, char type, int indexRequested, char* pathToFile);

/**
 * [Source : TP1 ex4] Permet de créer l'histogramme d'une image PGM (nuances de gris) donnée en argument, et de le stocker dans un tableau
 * retourné en fin de fonction pour pouvoir l'exploiter directement. L'histogramme sera aussi stocké dans le fichier pathToFile
 * si l'utilisateur souhaite l'afficher en utilisant par exemple GNUPLOT. Le curseur sera remis en début de document, et l'utilisateur
 * aura les droits de lecture sur ce fichier. Si un nom de fichier vide est donné, le fichier s'appellera 'histo.dat' dans le dossier courant.
 */
int* histogramme(OCTET* ImgIn, int imgWidth, int imgHeight, char* pathToFile);

/**
 * [Source : TP1 ex4] Permet de créer l'histogramme d'une image PPM (couleurs) donnée en argument, et de le stocker dans un tableau
 * retourné en fin de fonction pour pouvoir l'exploiter directement. L'histogramme sera aussi stocké dans le fichier pathToFile
 * si l'utilisateur souhaite l'afficher en utilisant par exemple GNUPLOT. Le curseur sera remis en début de document, et l'utilisateur
 * aura les droits de lecture sur ce fichier. Si un nom de fichier vide est donné, le fichier s'appellera 'histoRGB.dat' dans le dossier courant.
 */
int**histogrammeRGB(OCTET* ImgIn, int imgWidth, int imgHeight, char* pathToFile);

/**
 * [Source : TP2 ex1] Permet de copier une image PGM depuis le buffer `fileSource` vers le buffer `fileDest`. L'image sera de taille `size = width x height`.
 */
void copyImage(OCTET* fileSource, OCTET* fileDest, int width, int height);

/**
 * [Source : TP2 ex1] Permet de copier une image PPM depuis le buffer `fileSource` vers le buffer `fileDest`. L'image sera de taille `size = width x height`. Effectue la copie des 3 canaux de couleur rouge, vert et bleus.
 */
void copyImageRGB(OCTET* fileSource, OCTET* fileDest, int width, int height);

/**
 * [Source : TP2 ex1] Permet d'appliquer une dilatation ou érosion de l'image donnée par `in` et copiera le résultat dans `out`. Le kernel sera généré dans la fonction, et sera carré de taille `kernelSize * kernelSize`. En modifiant l'attribut `target`, on peut spécifier une érosion (`target = BLACK || 0` pour une dilatation du fond, et donc une érosion) ou une dilatation (`target = WHITE || 255` pour une dilatation du sujet, et donc une dilatation). Pour cela, le kernel sera non pas un kernel plein (toutes les cases du même type), mais sera un kernel en forme de losange (comme 4 voisins haut,bas,gauche,droite en 3*3). Si on veut spécifier un autre type de kernel, mieux vaut utiliser la fonction `applyKernel()`.
 */
void generateAndApplyMaskingKernel(OCTET* in, OCTET* out, int imgWidth, int imgHeight, int kernelSize, int target);

/**
 * [Source : TP2 ex1] Permet d'appliquer une dilatation ou érosion de l'image donnée par `in` et copiera le résultat dans `out`. ATTENTION : les images devront etre seuillées Le kernel sera donné par l'utilisateur dans l'arguement `kernel` et devra être carré de taille `kernelSize * kernelSize`. En modifiant l'attribut `target`, on peut spécifier une érosion (`target = BLACK || 0` pour une dilatation du fond, et donc une érosion) ou une dilatation (`target = WHITE || 255` pour une dilatation du sujet, et donc une dilatation). 
 */
void applyMaskingKernel(OCTET* in, OCTET* out, int imgWidth, int imgHeight, int** kernel, int kernelSize, int target);

/**
 * [Source : TP2 ex4] Permet de générer un contour de l'image donnée en premier argument ('image donnée doit être seuillée). Effectue une détection de contours grâce à une dilatation de l'image donnée. En effet, si des pixels d'une image dilatée sont différents que ceux de son image de référence, alors ceux ci doivent etre sur le bord d'une forme détectable. À L'inverse, si des pixels de l'image dilatée sont les mêmes dans l'image de référence et dans l'image dilatée, alors ceux ci appartiennent soit à un fond, soit au sujet, mais pas au contour de celui ci
 */
void genererContour(OCTET* in, OCTET* out, int imgWidth, int imgHeight);

/**
 * Génère un masque automatiquement de taille `n`. Ce masque sera de type 'voisins'. C.à.d que vu du dessus, il aura la forme d'un losange. Voir le corps de la fonction pour des exemples de masques de taille 3 et 5.
 */
int**generateMaskingKernel(int n);

#endif