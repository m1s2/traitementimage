#include <fcntl.h>
#include <stdio.h>
#include "../h/image_ppm.h"
#include "../h/tp.h"

void seuillage(OCTET* ImgIn, OCTET* ImgOut, int imgWidth, int imgHeight, int threshold) {
	for (int i = 0; i < imgHeight; ++i) {
		for (int j = 0; j < imgWidth; ++j) {
			ImgOut[i*imgWidth + j] = (ImgIn[i*imgWidth + j] <= threshold) ? 0 : 255;
		}
	}
	return;
}

void seuillageMultiple(OCTET* ImgIn, OCTET* ImgOut, int imgWidth, int imgHeight, int* thresholds, int quantityThresholds) {

	if (quantityThresholds != 4 && quantityThresholds != 3) {
		printf("ERROR : tp.c::seullageMultiple()\nCould not compute. Too many ceiling values or too few were provided.\n");
		return;
	}

	int oldVal, newVal;

	for (int i = 0; i < imgHeight; ++i) {
		for (int j = 0; j < imgWidth; ++j) {
			// prends la valeur actuelle du pixel
			oldVal = ImgIn[i*imgWidth + j];
			// Si en dessous du premier seuil,
			// la mettre a 0
			if (oldVal <= thresholds[0]) {
				newVal = 0;
			}
			// Si on a donné 3 valeurs de seuil,
			// mettre deux seuils différents
			if (quantityThresholds == 3) {
				// Seuil 2
				if (oldVal > thresholds[0] && oldVal <= thresholds[1]) {
					newVal = 128;
				}
				// Seuil 3
				if (oldVal > thresholds[1]) {
					newVal = 255;
				}
			} else {
				if (oldVal > thresholds[0] && oldVal <= thresholds[1]) {
					newVal = 85;
				}
				if (oldVal > thresholds[1] && oldVal <= thresholds[2]) {
					newVal = 170;
				}
				if (oldVal > thresholds[2]) {
					newVal = 255;
				}
			}
			ImgOut[i*imgWidth + j] = newVal;
		}
	}
}

void profilComposanteImage(OCTET* ImgIn, int imgWidth, int imgHeight, char type, int indexRequested, char* pathToFile) {

	// Types of profile able to be done.
	enum typeOfProfile {
		LINES,
		COLUMNS
	};
	
	// Démarre en assumant un profil de ligne.
	int profileType = 0;

	// Vérifie l'argument utilisateur du type de profil demandé.
	if (type != 'c' && type != 'l') {
		printf("ERROR : tp.c::profilComposanteImage()\nCould not assess the type of profile wanted by the user.\nWill not perform anything.\n");
	} else if (type == 'c') {
		profileType = 1;
	}

	int oldVal, upperBound;

	// On vérifie que l'index demandé est bien présent dans l'image.
	if (profileType == COLUMNS) {
		if (indexRequested >= imgWidth) {
			printf("ERROR : tp.c::profilComposanteImage()\nCould not plot the profile of the column %i, index out of range.", indexRequested);
		} else {
			upperBound = indexRequested;
		}
	} else {
		if (indexRequested >= imgHeight) {
			printf("ERROR : tp.c::profilComposanteImage()\nCould not plot the profile of the line %i, index out of range.", indexRequested);
		} else {
			upperBound = indexRequested;
		}
	}

	FILE* proFile = fopen((const char*)pathToFile, (const char*)"w");

	// Prends les valeurs données
	for (int i = 0; i < upperBound; ++i) {
                // prends la valeur actuelle du pixel
                oldVal = (profileType == COLUMNS) ? ImgIn[i*imgWidth+indexRequested] : ImgIn[indexRequested*imgWidth+i];
                // L'imprime dans le fichier profile.dat
                fprintf(proFile, "%i\n", oldVal);
	}

	fclose(proFile);
}

int* histogramme(OCTET* ImgIn, int imgWidth, int imgHeight, char* pathToFile) {
	// Calcule taille image donnée :
	int imgSize = imgWidth * imgHeight;
	// Alloue un tableau (256 valeurs de gris par défaut) :
	int* histoValues = (int*)calloc(256,sizeof(int));

	// Calcule l'histogramme
	for (int i = 0; i < imgSize; ++i) {
                histoValues[ImgIn[i]]++;
	}

	// Ecrit dans un fichier
	if (strcmp((const char*)pathToFile, "") == 0) 
		pathToFile = "./histo.dat";
	FILE* histoFile = fopen((const char*)pathToFile, "w");
	if (histoFile != NULL) {
		for (int i = 0; i < 256; ++i) {
			fprintf(histoFile, "%i\n", histoValues[i]);
		}
		fclose(histoFile);
	} else {
		printf("Error : tp.c::histogramme()\nCould not open the file requested. Histogram will not be written to an external file.\n");
	}

	return histoValues;
}

int**histogrammeRGB(OCTET* ImgIn, int imgWidth, int imgHeight, char* pathToFile) {
	// Taille d'image sur 3 canaux couleurs
	int i3 = imgHeight * imgWidth * 3;

	int* histoValuesR = (int*)calloc(256,sizeof(int));
	int* histoValuesV = (int*)calloc(256,sizeof(int));
	int* histoValuesB = (int*)calloc(256,sizeof(int));

	// Prends les valeurs données et ajoute
	// 1 a la bonne case de l'histogramme
	// [imgSize*3 car 3 channel de couleurs]
	for (int i = 0; i < i3; i+=3) {
		histoValuesR[ImgIn[i+0]]++;
		histoValuesV[ImgIn[i+1]]++;
		histoValuesB[ImgIn[i+2]]++;
	}

	// Si aucun chemin donné, extraire données dans histoRGB.dat
	if (strcmp((const char*)pathToFile, "") == 0) 
		pathToFile = "./histoRGB.dat";
	FILE* histoFile = fopen((const char*)pathToFile, "w");
	if (histoFile != NULL) {
		for (int i = 0; i < 256; ++i) {
			fprintf(histoFile, "%i %i %i\n", histoValuesR[i], histoValuesV[i], histoValuesB[i]);
		}
		fclose(histoFile);
	} else {
		printf("Error : tp.c::histogramme()\nCould not open the file requested. Histogram will not be written to an external file.\n");
	}

	int** fullHisto = (int**)malloc(3*sizeof(int*));
	fullHisto[0] = histoValuesR;
	fullHisto[1] = histoValuesV;
	fullHisto[2] = histoValuesB;
	return fullHisto;
}

void copyImage(OCTET* fileSource, OCTET* fileDest, int w, int h) {
	// Simple procédure de copie. On parcours
	// l'image en entier, et ensuite on copie
	// chaque pixel gris depuis IN vers OUT.
	
	for (int i = 0; i < h; ++i)
		for (int j = 0; j < w; ++j)
			fileDest[i*w + j] = fileSource[i*w + j];
}

void copyImageRGB(OCTET* fileSource, OCTET* fileDest, int w, int h) {
	// Simple procédure de copie. On parcours
	// l'image en entier, et ensuite on copie
	// chaque pixel R,G,B depuis IN vers OUT.

	for (int i = 0; i < h; ++i)
		for (int j = 0; j < w*3; ++j)
			fileDest[i*w + j] = fileSource[i*w + j];
}

void generateAndApplyMaskingKernel(OCTET* in, OCTET* out, int imgWidth, int imgHeight, int kernelSize, int target) {
	// Generer le kernel
	int** kernel = generateMaskingKernel(kernelSize);

	// Prendre le centre du kernel :
	int kernelCenter = (kernelSize % 2 == 1) ? (kernelSize + 1)/2 - 1 : kernelSize/2 - 1;

	// Prends les valeurs a appliquer pour le fond :
	int background = (target == WHITE) ? BLACK : WHITE;

	// Parcours de l'image :
	for (int i = 0; i < imgHeight; ++i) {
		for (int j = 0; j < imgWidth; ++j) {
			// On verifie les cases du kernel par rapport aux cases du fond/sujet :
			//
			// On doit aussi vérifier que le kernel ne dépasse pas de l'image, 
			// auquel cas il faudra vérifier uniquement les cases dans l'image :
			
			int xOffset, yOffset;

			int corresponds = 1;

			// Parcours depuis le centre du kernel jusqu'a ses extrémités
			//
			// X varie de haut en bas, Y de gauche à droite
			//
			// Pour tous les X de chaque cotés du centre kernel
			for (int x = kernelCenter; x >= 0; --x) {
				// Calculer la distance kernel : (toujours positive)
				xOffset = kernelCenter - x;
				// Si on dépasse pas le bord gauche de l'image
				if (j-xOffset >= 0) {
					// Pour tous les Y de chaque coté du centre kernel
					for (int y = kernelCenter; y >= 0; --y) {
						// Calculer la distance du centre kernel
						yOffset = kernelCenter - y;
						// Si on dépasse le haut de l'image, et que le kernel est censé masquer :
						if (i-yOffset >= 0 && kernel[kernelCenter - yOffset][kernelCenter - xOffset] == OPAQUE) {
							corresponds = corresponds && (in[(i-yOffset)*imgWidth + (j-xOffset)] == target) ? 1 : 0;
						}
						// Sinon, si on dépasse pas le bas de l'image, et que le kernel masque :
						if (i+yOffset <= imgHeight && kernel[kernelCenter + yOffset][kernelCenter - xOffset] == OPAQUE) {
							corresponds = corresponds && (in[(i+yOffset)*imgWidth + (j-xOffset)] == target) ? 1 : 0;
						}
					}
				} 
				// Si on ne dépasse pas le bord droit de l'image
				if (j+xOffset <= imgHeight) {
					// Pour tous les Y de chaque côté du centre kernel :
					for (int y = kernelCenter; y >= 0; --y) {
						// Calculer la distance du centre kernel :
						yOffset = kernelCenter - y;
						// Si on ne dépasse pas le haut de l'image, et que le kernel est censé masquer :
						if (i-yOffset >= 0 && kernel[kernelCenter - yOffset][kernelCenter + xOffset] == OPAQUE) {
							corresponds = corresponds && (in[(i-yOffset)*imgWidth + (j+xOffset)] == target) ? 1 : 0;
						}
						// Sinon, si on dépasse pas le bas de l'image, et que le kernel masque :
						if (i+yOffset <= imgHeight && kernel[kernelCenter + yOffset][kernelCenter + xOffset] == OPAQUE) {
							corresponds = corresponds && (in[(i+yOffset)*imgWidth + (j+xOffset)] == target) ? 1 : 0;
						}
					}
				}
			}

			if (corresponds == 1) {
				out[i*imgWidth + j] = target;
			} else {
				out[i*imgWidth + j] = background;
			}
		}
	}
}

void applyMaskingKernel(OCTET* in, OCTET* out, int imgWidth, int imgHeight, int** kernel, int kernelSize, int target) {
	// Prendre le centre du kernel :
	int kernelCenter = (kernelSize % 2 == 1) ? (kernelSize + 1)/2 - 1 : kernelSize/2 - 1;

	// Prends les valeurs a appliquer pour le fond :
	int background = (target == WHITE) ? BLACK : WHITE;

	// Parcours de l'image :
	for (int i = 0; i < imgHeight; ++i) {
		for (int j = 0; j < imgWidth; ++j) {
			// On verifie les cases du kernel par rapport aux cases du fond/sujet :
			//
			// On doit aussi vérifier que le kernel ne dépasse pas de l'image, 
			// auquel cas il faudra vérifier uniquement les cases dans l'image :
			
			int xOffset, yOffset;

			int corresponds = 1;

			// Parcours depuis le centre du kernel jusqu'a ses extrémités
			//
			// X varie de haut en bas, Y de gauche à droite
			//
			// Pour tous les X de chaque cotés du centre kernel
			for (int x = kernelCenter; x >= 0; --x) {
				// Calculer la distance kernel : (toujours positive)
				xOffset = kernelCenter - x;
				// Si on dépasse pas le bord gauche de l'image
				if (j-xOffset >= 0) {
					// Pour tous les Y de chaque coté du centre kernel
					for (int y = kernelCenter; y >= 0; --y) {
						// Calculer la distance du centre kernel
						yOffset = kernelCenter - y;
						// Si on dépasse le haut de l'image, et que le kernel est censé masquer :
						if (i-yOffset >= 0 && kernel[kernelCenter - yOffset][kernelCenter - xOffset] == OPAQUE) {
							corresponds = corresponds && (in[(i-yOffset)*imgWidth + (j-xOffset)] == target) ? 1 : 0;
						}
						// Sinon, si on dépasse pas le bas de l'image, et que le kernel masque :
						if (i+yOffset <= imgHeight && kernel[kernelCenter + yOffset][kernelCenter - xOffset] == OPAQUE) {
							corresponds = corresponds && (in[(i+yOffset)*imgWidth + (j-xOffset)] == target) ? 1 : 0;
						}
					}
				} 
				// Si on ne dépasse pas le bord droit de l'image
				if (j+xOffset <= imgHeight) {
					// Pour tous les Y de chaque côté du centre kernel :
					for (int y = kernelCenter; y >= 0; --y) {
						// Calculer la distance du centre kernel :
						yOffset = kernelCenter - y;
						// Si on ne dépasse pas le haut de l'image, et que le kernel est censé masquer :
						if (i-yOffset >= 0 && kernel[kernelCenter - yOffset][kernelCenter + xOffset] == OPAQUE) {
							corresponds = corresponds && (in[(i-yOffset)*imgWidth + (j+xOffset)] == target) ? 1 : 0;
						}
						// Sinon, si on dépasse pas le bas de l'image, et que le kernel masque :
						if (i+yOffset <= imgHeight && kernel[kernelCenter + yOffset][kernelCenter + xOffset] == OPAQUE) {
							corresponds = corresponds && (in[(i+yOffset)*imgWidth + (j+xOffset)] == target) ? 1 : 0;
						}
					}
				}
			}
			// If the kernel is applicable here :
			if (corresponds == 1) {
				out[i*imgWidth + j] = target;
			} else {
				// If not, then is is part of the background
				out[i*imgWidth + j] = background;
			}
		}
	}
}

void genererContour(OCTET* in, OCTET* out, int imgWidth, int imgHeight) {
	// Generer une image pour la dilation temporaire :
	OCTET* ImgDil;
	// Prendre la taille de l'image
	int imgSize = imgWidth * imgHeight;
	// Allouer le tableau pour l'image temporaire,
	allocation_tableau(ImgDil, OCTET, imgSize);
	// et ensuite faire une copie de l'image actuelle :
	copyImage(in, ImgDil, imgWidth, imgHeight);

	// Dilater l'image par le kernel des voisins de taille 5
	generateAndApplyMaskingKernel(in, ImgDil, imgWidth, imgHeight, 5, WHITE);

	// Pour tous les pixels de l'image :
	for (int x = 0; x < imgWidth; ++x) {
		for (int y = 0; y < imgHeight; ++y) {
			// Si les deux pixels sont différents
			if (in[y*imgWidth + x] != ImgDil[y*imgWidth + x]) {
				// Alors un contour :
				out[y*imgWidth + x] = 0;
			} else {
				// Sinon, alors ce n'est pas une forme
				// détectable
				out[y*imgWidth + x] = 255;
			}
		}
	}
}

int**generateMaskingKernel(int n) {
	// NOTE : Genere un kernel de type 'voisins',
	// c.a.d un kernel qui remplit un losange vu
	// du dessus. Exemple pour n = 3 :
	//
	//          [ O X O ]
	// kernel = [ X X X ]
	//          [ O X O ]
	//
	// Exemple pour n = 5 :
	//
	//	    [ O O X O O ]
	//	    [ O X X X O ]
	// kernel = [ X X X X X ]
	//	    [ O X X X O ]
	//	    [ O O X O O ]
	
	// Permet de générer toujours un kernel de taille impaire
	// (plus facile pour l'appliquer après coup)
	int kernelSize = (n % 2 == 0) ? n+1 : n;

	// n est la taille kernel à générer
	int ** kernel = calloc(kernelSize,sizeof(int*));
	for (int i = 0; i < kernelSize; ++i) {
		kernel[i] = calloc(kernelSize,sizeof(int));
	}

	// Parcours du kernel pour le remplir 
	for (int i = 0; i < kernelSize; ++i) {
		// Index maximum et minimum du 'losange' à faire
		int lowerBound = (((kernelSize+1)/2)-1)-i;
		int upperBound = (((kernelSize+1)/2)-1)+i;
		for (int j = 0; j < kernelSize; ++j) {
			if (j >= lowerBound && j <= upperBound) {
				// Ne laissera pas passer le sujet
				kernel[i][j] = OPAQUE;
			} else {
				// N'affectera pas les pixels sous 
				// ce bout du kernel.
				kernel[i][j] = TRANSPARENT;
			}
		}
	}

	return kernel;
}
