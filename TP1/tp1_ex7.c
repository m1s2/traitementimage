#include <stdio.h>
#include <fcntl.h>
#include <math.h>
#include "../lib/h/image_ppm.h"

int average(OCTET* ImgIn, int imgSize);
int median(OCTET* ImgIn, int imgSize);
int peak(OCTET* ImgIn, int imgSize);

int main(int argc, char* argv[]) {
	if (argc != 4) {
		printf("Usage : \n\tseuilAuto ImageIn.ppm ImageOut.pgm metric\n\n");
		printf("Ou metrix est une des valeurs suivantes : average, median, peak, \net definit la metrique utilisee pour determinaer le seuil optimal\n\n");
		exit(1);
	} else if (strcmp((const char*)"--usage", (const char*)argv[1]) == 0) {
		printf("Usage : \n\tseuilAuto ImageIn.ppm ImageOut.pgm metric\n\n");
		printf("Ou metrix est une des valeurs suivantes : average, median, peak, \net definit la metrique utilisee pour determinaer le seuil optimal\n\n");
		exit(1);
	}

	printf("Attention, ce programme assume que vous connaissiez son utilisation. Très peu de tests\n");
	printf("sont mis en place pour éviter ce programme de ne pas fonctionner.\n");
	printf("[ Vous pouvez y acceder grace a l'option --usage ]\n");

	// Arguments nécessaires au programme
	char nomFichierEntree[255];
	char nomFichierSortie[255];
	int imgHeight;
	int imgWidth;
	int imgSize;
	int (*ceilFunction)(OCTET*,int);

	// Prise des noms de fichier pour 
	// la lecture de fichiers
	sscanf (argv[1],"%s",nomFichierEntree);
	sscanf (argv[2],"%s",nomFichierSortie);

	// Prendre taille image
	lire_nb_lignes_colonnes_image_pgm(nomFichierEntree, &imgHeight,&imgWidth);
	imgSize = imgHeight * imgWidth;

	// Creer fichiers entree sortie
	OCTET* ImgIn;
	OCTET* ImgOut;

	// Lire fichier donné en argument
	allocation_tableau(ImgIn, OCTET, imgSize);
	lire_image_pgm(nomFichierEntree, ImgIn, imgSize);
	allocation_tableau(ImgOut, OCTET, imgSize);

	if (strcmp(argv[3], (const char*)"average") == 0) {
		ceilFunction = &average;
	} else if (strcmp(argv[3], (const char*)"median") == 0) {
		ceilFunction = &median;
	} else if (strcmp(argv[3], (const char*)"peak") == 0) {
		ceilFunction = &peak;
	} else {
		printf("Argument de fonction non compris, arret programme.\n");
		exit(2);
	}

	int imgCeil = (*ceilFunction)(ImgIn, imgSize);

	// Actions de "seuillage"
	for (int i = 0; i < imgHeight; ++i) {
		for (int j = 0; j < imgWidth; ++j) {
			ImgOut[i*imgWidth + j] = (ImgIn[i*imgWidth + j] <= imgCeil) ? 0 : 255;
		}
	}

	ecrire_image_pgm(nomFichierSortie, ImgOut, imgHeight, imgWidth);
	free(ImgIn);
	free(ImgOut);

	return 0;
}

int average(OCTET* ImgIn, int imgSize) {
	double avg = 0.0;

	// fais la somme des valeurs de pixels
	for (int i = 0; i < imgSize; ++i) {
		avg += ImgIn[i];
	}

	// prends la moyenne
	avg = avg/(double)imgSize;

	// retourne la valeur de seuil optimale
	return (int)round(avg);
}

int median(OCTET* ImgIn, int imgSize) {
	// on doit atteindre la valeur mediane des
	// valeurs de gris de l'image :
	int median_val = imgSize/2;

	// on fait l'histogramme de l'image :

        // Alloue un espace memoire de 256 cases de la taille
        // d'un entier, pour stocker les valeurs de l'histogramme
        int* histoValues = calloc((size_t)256, sizeof(int));

	// Prends les valeurs données et ajoute
        // 1 a la bonne case de l'histogramme
	for (int i = 0; i < imgSize; ++i) {
                histoValues[ImgIn[i]]++;
	}

	int optimalCeil = 0;
	int valuesPassed = 0;
	for (int i = 0; i < 256; ++i) {
		if ( valuesPassed + histoValues[i] > median_val ) {
			return optimalCeil;
		} else {
			optimalCeil = i;
			valuesPassed += histoValues[i];
		}
	}
}

int peak(OCTET* ImgIn, int imgSize) {
	// on fait l'histogramme de l'image :

        // Alloue un espace memoire de 256 cases de la taille
        // d'un entier, pour stocker les valeurs de l'histogramme
        int* histoValues = calloc((size_t)256, sizeof(int));

	// Prends les valeurs données et ajoute
        // 1 a la bonne case de l'histogramme
	for (int i = 0; i < imgSize; ++i) {
                histoValues[ImgIn[i]]++;
	}

	// on veut atteindre le pic de valeurs de gris :
	int peakValue = 0;
	int currentMax = 0;
	for (int i = 0; i < 256; ++i) {
		if ( peakValue < histoValues[i] ) {
			peakValue = histoValues[i];
			currentMax = i;
		}
	}

	return currentMax;
}