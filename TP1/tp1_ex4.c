#include <stdio.h>
#include <fcntl.h>
#include "../lib/h/image_ppm.h"

int main(int argc, char* argv[]) {
	if (argc != 2) {
		printf("Usage : \n\timageHisto ImageIn.pgm\n\n");
		exit(1);
	} else if (strcmp((const char*)"--usage", (const char*)argv[1]) == 0) {
		printf("Usage : \n\timageHisto ImageIn.pgm\n\n");
		exit(1);
	}

	printf("Attention, ce programme assume que vous connaissiez son utilisation. Très peu de tests\n");
	printf("sont mis en place pour éviter ce programme de ne pas fonctionner.\n");
	printf("[ Vous pouvez y acceder grace a l'option --usage ]\n");

	// Arguments nécessaires au programme
	char nomFichierEntree[255];
	int imgHeight;
	int imgWidth;
	int imgSize;
        // Alloue un espace memoire de 256 cases de la taille
        // d'un entier, pour stocker les valeurs de l'histogramme
        int* histoValues = calloc((size_t)256, sizeof(int));

	// Prise des noms de fichier pour 
	// la lecture de fichiers
	sscanf (argv[1],"%s",nomFichierEntree);

	// Prendre taille image
	lire_nb_lignes_colonnes_image_pgm(nomFichierEntree, &imgHeight,&imgWidth);
	imgSize = imgHeight * imgWidth;

	OCTET* ImgIn;
        FILE* proFile = fopen("histo.dat", "w");

	// Lire fichier donné en argument
	allocation_tableau(ImgIn, OCTET, imgSize);
	lire_image_pgm(nomFichierEntree, ImgIn, imgSize);

	// Prends les valeurs données et ajoute
        // 1 a la bonne case de l'histogramme
	for (int i = 0; i < imgSize; ++i) {
                histoValues[ImgIn[i]]++;
	}

        // Ecrit les occurences des 256 valeurs de gris
        // dans un fichier nommé histo.dat :
        for (int i = 0; i < 256; ++i) {
                fprintf(proFile, "%i %i\n", i, histoValues[i]);
        }

        fclose(proFile);

	free(ImgIn);

	return 0;
}