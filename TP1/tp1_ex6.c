#include <stdio.h>
#include <fcntl.h>
#include "../lib/h/image_ppm.h"

int main(int argc, char* argv[]) {
	if (argc != 6) {
		printf("Usage : \n\tseuilsCouleur ImageIn.ppm ImageOut.ppm seuilR seuilG seuilB\n\n");
		exit(1);
	} else if (strcmp((const char*)"--usage", (const char*)argv[1]) == 0) {
		printf("Usage : \n\tseuilsCouleur ImageIn.ppm ImageOut.ppm seuilR seuilG seuilB\n\n");
		exit(1);
	}

	printf("Attention, ce programme assume que vous connaissiez son utilisation. Très peu de tests\n");
	printf("sont mis en place pour éviter ce programme de ne pas fonctionner.\n");
	printf("[ Vous pouvez y acceder grace a l'option --usage ]\n");

	// Arguments nécessaires au programme
	char nomFichierEntree[255];
	char nomFichierSortie[255];
	int imgHeight;
	int imgWidth;
	int imgSize;
        int seuilR = atoi(argv[3]);
        int seuilG = atoi(argv[4]);
        int seuilB = atoi(argv[5]);

	// Prise des noms de fichier pour 
	// la lecture de fichiers
	sscanf (argv[1],"%s",nomFichierEntree);
	sscanf (argv[2],"%s",nomFichierSortie);

	// Prendre taille image
	lire_nb_lignes_colonnes_image_ppm(nomFichierEntree, &imgHeight, &imgWidth);
	imgSize = imgHeight * imgWidth;
	int i3 = imgSize*3;

	OCTET* ImgIn;
	OCTET* ImgOut;

	// Lire fichier donné en argument
	allocation_tableau(ImgIn, OCTET, i3);
	lire_image_ppm(nomFichierEntree, ImgIn, imgSize);
	allocation_tableau(ImgOut, OCTET, i3);

	// Prends les valeurs données et ajoute
	// 1 a la bonne case de l'histogramme
	// imgSize*3 car 3 channel de couleurs
	for (int i = 0; i < imgHeight; ++i) {
                for (int j = 0; j < imgWidth*3; j+=3) {
                        ImgOut[imgWidth*3*i + j + 0] = (ImgIn[imgWidth*3*i + j + 0] <= seuilR) ? 0 : 255;
                        ImgOut[imgWidth*3*i + j + 1] = (ImgIn[imgWidth*3*i + j + 1] <= seuilG) ? 0 : 255;
                        ImgOut[imgWidth*3*i + j + 2] = (ImgIn[imgWidth*3*i + j + 2] <= seuilB) ? 0 : 255;
                }
	}

        ecrire_image_ppm(nomFichierSortie, ImgOut, imgHeight, imgWidth);
	free(ImgIn);
	free(ImgOut);

	return 0;
}