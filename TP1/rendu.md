# Traitement des images : TP1

----

N.B. : J'ai ecrit ce rapport sur mon clavier personnel, qui est en QWERTY. Ce rapport ne contiendra donc aucun accent. Desole pour l'inconvenience, ainsi que pour le massacre de la langue francaise qui va s'ensuivre.

### ex1 : Seuils d'une image de niveaux de gris

Dans cet exercice, il fallait simplement effectuer un seuillage de niveaux de gris sur une image PGM. Pour cela, j'ai utilise le bout d'algorithme donne dans votre sujet de TP. Par exemple, pour l'image 01.pgm, nous obtenons le resultat suivant avec un seuil de 100, 128, et 200 :

![test](01_ceil_100.jpg "test")