#include <stdio.h>
#include "../lib/h/image_ppm.h"

int* get2Seuils(char*,char*);
int* get3Seuils(char*,char*,char*);

int main(int argc, char* argv[]) {
	if (argc != 6 && argc != 7) {
		printf("Usage : \n\tseuilsMult ImageIn.pgm ImageOut.pgm nbSeuils seuil1 seuil2 [seuil3]\n\n");
		printf("Avec nbSeuils entre 3 et 4, et seuil1 et seuil2 compris entre 0 et 255.\n");
		exit(1);
	} else if (strcmp((const char*)"--usage", (const char*)argv[1]) == 0) {
		printf("Usage : \n\tseuilsMult ImageIn.pgm ImageOut.pgm nbSeuils seuil1 seuil2 [seuil3]\n\n");
		printf("Avec nbSeuils entre 3 et 4, et seuil1 et seuil2 compris entre 0 et 255.\n");
		exit(1);
	}

	printf("Attention, ce programme assume que vous connaissiez son utilisation. Très peu de tests\n");
	printf("sont mis en place pour éviter ce programme de ne pas fonctionner.\n");
	printf("[ Vous pouvez y acceder grace a l'option --usage ]\n");

	// Arguments nécessaires au programme
	char nomFichierEntree[255];
	char nomFichierSortie[255];
	int imgHeight;
	int imgWidth;
	int imgSize;
	int nbCeils = atoi(argv[3]);

	// Verification des arguments programme
	if ((nbCeils == 0 && argv[3] != "0") || nbCeils < 3 || nbCeils > 4) {
		printf("Mauvais argument de seuil. Arret programme.\n");
		exit(2);
	}
	
	// Prise des noms de fichier pour 
	// la lecture et l'ecriture
	sscanf (argv[1],"%s",nomFichierEntree);
	sscanf (argv[2],"%s",nomFichierSortie);

	int* imgCeils;
	if (nbCeils == 3) {
		imgCeils = get2Seuils(argv[4], argv[5]);
	} else {
		imgCeils = get3Seuils(argv[4], argv[5], argv[6]);
	}


	OCTET *ImgIn;
	OCTET *ImgOut;

	// Prendre taille image
	lire_nb_lignes_colonnes_image_pgm(nomFichierEntree, &imgHeight,&imgWidth);
	imgSize = imgHeight * imgWidth;

	// Lire fichier donné en argument
	allocation_tableau(ImgIn, OCTET, imgSize);
	lire_image_pgm(nomFichierEntree, ImgIn, imgSize);
	allocation_tableau(ImgOut, OCTET, imgSize);

	int oldVal, newVal;

	// Actions de "seuillage"
	// i controle la ligne,
	// j controle la colonne.
	for (int i = 0; i < imgHeight; ++i) {
		for (int j = 0; j < imgWidth; ++j) {
			// prends la valeur actuelle du pixel
			oldVal = ImgIn[i*imgWidth + j];
			// Si en dessous du premier seuil,
			// la mettre a 0
			if (oldVal <= imgCeils[0]) {
				newVal = 0;
			}
			// Si on a donné 3 valeurs de seuil,
			// mettre deux seuils différents
			if (nbCeils == 3) {
				// Seuil 2
				if (oldVal > imgCeils[0] && oldVal <= imgCeils[1]) {
					newVal = 128;
				}
				// Seuil 3
				if (oldVal > imgCeils[1]) {
					newVal = 255;
				}
			} else {
				if (oldVal > imgCeils[0] && oldVal <= imgCeils[1]) {
					newVal = 85;
				}
				if (oldVal > imgCeils[1] && oldVal <= imgCeils[2]) {
					newVal = 170;
				}
				if (oldVal > imgCeils[2]) {
					newVal = 255;
				}
			}
			ImgOut[i*imgWidth + j] = newVal;
		}
	}

	// Ecriture image
	ecrire_image_pgm(nomFichierSortie, ImgOut, imgHeight, imgWidth);
	free(ImgIn);
	free(ImgOut);
	free(imgCeils);

	return 0;
}

int* get2Seuils(char* str1, char* str2) {
	int c1 = atoi(str1);
	int c2 = atoi(str2);
	
	if ((c1 == 0 && str1 != "0") || c1 < 0 || c1 > 255) {
		printf("Mauvais argument de seuil. Arret programme.\n");
		exit(2);
	}
	if ((c2 == 0 && str2 != "0") || c2 < 0 || c2 > 255) {
		printf("Mauvais argument de seuil. Arret programme.\n");
		exit(2);
	}

	int* tab = malloc(2*sizeof(int));

	tab[0] = (c1 < c2) ? c1 : c2;
	tab[1] = (c1 > c2) ? c1 : c2;

	printf("Order : %i %i\n", tab[0], tab[1]);

	return tab;
}

int* get3Seuils(char* str1, char* str2, char* str3) {
	int c1 = atoi(str1);
	int c2 = atoi(str2);
	int c3 = atoi(str3);
	
	if ((c1 == 0 && str1 != "0") || c1 < 0 || c1 > 255) {
		printf("Mauvais argument de seuil. Arret programme.\n");
		exit(2);
	}
	if ((c2 == 0 && str2 != "0") || c2 < 0 || c2 > 255) {
		printf("Mauvais argument de seuil. Arret programme.\n");
		exit(2);
	}
	if ((c3 == 0 && str3 != "0") || c3 < 0 || c3 > 255) {
		printf("Mauvais argument de seuil. Arret programme.\n");
		exit(2);
	}

	int* tab = malloc(3*sizeof(int));

	tab[0] = (c1 < c2) ? (c3 < c1) ? c3 : c1 : (c3 < c2) ? c3 : c2;
	tab[1] = (c1 < c2) ? (c3 > c1) ? (c3 < c2) ? c3 : c2 : c1 : (c3 > c2) ? (c3 < c1) ? c3 : c1 : c3;
	tab[2] = (c1 > c2) ? (c3 > c1) ? c3 : c1 : (c3 > c2) ? c3 : c2;

	printf("Order : %i %i %i\n", tab[0], tab[1], tab[2]);

	return tab;
}