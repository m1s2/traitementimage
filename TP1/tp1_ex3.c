#include <stdio.h>
#include <fcntl.h>
#include "../lib/h/image_ppm.h"

typedef enum typeAskedFromUser {
        COLUMNS,
        LINES
} kind;

int main(int argc, char* argv[]) {
	if (argc != 4) {
		printf("Usage : \n\timageProfile ImageIn.pgm type number\n\n");
		printf("Avec type c pour colonne, l pour ligne et number un numéro de ligne ou colonne de l'image.\n");
		exit(1);
	} else if (strcmp((const char*)"--usage", (const char*)argv[1]) == 0) {
		printf("Usage : \n\timageProfile ImageIn.pgm type number\n\n");
		printf("Avec type c pour colonne, l pour ligne et number un numéro de ligne ou colonne de l'image.\n");
		exit(1);
	}

	printf("Attention, ce programme assume que vous connaissiez son utilisation. Très peu de tests\n");
	printf("sont mis en place pour éviter ce programme de ne pas fonctionner.\n");
	printf("[ Vous pouvez y acceder grace a l'option --usage ]\n");

	// Arguments nécessaires au programme
	char nomFichierEntree[255];
	int imgHeight;
	int imgWidth;
	int imgSize;
        int askedProfile = atoi(argv[3]);

	// Prise des noms de fichier pour 
	// la lecture de fichiers
	sscanf (argv[1],"%s",nomFichierEntree);

        kind profileType;

        // Prise en compte du type de profil
        // demandé par l'utilisateur
        if (strcmp(argv[2], (const char*)"c") == 0) {
                profileType = COLUMNS;
        } else if (strcmp(argv[2], (const char*)"l") == 0) {
                profileType = LINES;
        } else {
                printf("Type de profil non reconnu. Exiting program ...\n");
                exit(2);
        }

	// Prendre taille image
	lire_nb_lignes_colonnes_image_pgm(nomFichierEntree, &imgHeight,&imgWidth);
	imgSize = imgHeight * imgWidth;

	// Verification des arguments programme
	if ((askedProfile == 0 && argv[3] != "0") || (profileType == COLUMNS && askedProfile > imgWidth) || (profileType == LINES && askedProfile > imgHeight)) {
		printf("Mauvais argument de ligne. Arret programme.\n");
		exit(2);
	}

	OCTET* ImgIn;
        FILE* proFile = fopen("profile.dat", "w");

	// Lire fichier donné en argument
	allocation_tableau(ImgIn, OCTET, imgSize);
	lire_image_pgm(nomFichierEntree, ImgIn, imgSize);

	int oldVal;
        // Prends le nombre de valeurs a profiler pour
        // l'image en cours de lecture selon le type demandé
        int upperBound = (profileType == COLUMNS) ? imgHeight : imgWidth;

	// Prends les valeurs données
	for (int i = 0; i < upperBound; ++i) {
                // prends la valeur actuelle du pixel
                oldVal = (profileType == COLUMNS) ? ImgIn[i*imgWidth+askedProfile] : ImgIn[askedProfile*imgWidth+i];
                // L'imprime dans le fichier profile.dat
                fprintf(proFile, "%i\n", oldVal);
	}

        fclose(proFile);

	free(ImgIn);

	return 0;
}