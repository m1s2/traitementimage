#include <stdio.h>
#include "../lib/h/image_ppm.h"

int main(int argc, char* argv[]) {
	if (argc != 4) {
		printf("Usage : \n\tseuilGris ImageIn.pgm ImageOut.pgm seuil\n");
		printf("Avec seuil compris entre 0 et 255.\n");
		exit(1);
	} else if (strcmp((const char*)"--usage", (const char*)argv[1]) == 0) {
		printf("Usage : \n\tseuilGris ImageIn.pgm ImageOut.pgm seuil\n");
		printf("Avec seuil compris entre 0 et 255.\n");
		exit(1);
	}

	printf("Attention, ce programme assume que vous connaissiez son utilisation. Très peu de tests\n");
	printf("sont mis en place pour éviter ce programme de ne pas fonctionner.\n");
	printf("[ Vous pouvez y acceder grace a l'option --usage ]\n");

	// Arguments nécessaires au programme
	char nomFichierEntree[255];
	char nomFichierSortie[255];
	int imgHeight;
	int imgWidth;
	int imgSize;
	int imgCeil = atoi(argv[3]);

	sscanf (argv[1],"%s",nomFichierEntree) ;
	sscanf (argv[2],"%s",nomFichierSortie);

	if ((imgCeil == 0 && argv[3] != "0") || imgCeil < 0 || imgCeil > 255) {
		printf("Mauvais argument de seuil. Arret programme.\n");
		exit(2);
	}
	
	OCTET *ImgIn;
	OCTET *ImgOut;

	// Prendre taille image
	lire_nb_lignes_colonnes_image_pgm(nomFichierEntree, &imgHeight,&imgWidth);
	imgSize = imgHeight * imgWidth;

	// Lire fichier donné en argument
	allocation_tableau(ImgIn, OCTET, imgSize);
	lire_image_pgm(nomFichierEntree, ImgIn, imgSize);
	allocation_tableau(ImgOut, OCTET, imgSize);

	// Actions de "seuillage"
	for (int i = 0; i < imgHeight; ++i) {
		for (int j = 0; j < imgWidth; ++j) {
			ImgOut[i*imgWidth + j] = (ImgIn[i*imgWidth + j] <= imgCeil) ? 0 : 255;
		}
	}

	// Ecriture image
	ecrire_image_pgm(nomFichierSortie, ImgOut, imgHeight, imgWidth);
	free(ImgIn);
	free(ImgOut);

	return 0;
}