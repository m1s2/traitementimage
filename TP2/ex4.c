#include <stdio.h>
#include "../lib/h/image_ppm.h"

#define KERNEL_SIZE_X	3
#define KERNEL_SIZE_Y	3

void copyImage(OCTET*, OCTET*, int, int);

int main(int argc, char* argv[]) {
	if (argc != 4) {
		printf("Usage : \n\tdifference Image1.pgm Image2.pgm ImageOut.pgm\n\n");
		exit(1);
	} else if (strcmp((const char*)"--usage", (const char*)argv[1]) == 0) {
		printf("Usage : \n\tdifference Image1.pgm Image2.pgm ImageOut.pgm\n\n");
		exit(1);
	}

	// Arguments nécessaires au programme
	char nomFichier1Entree[255];
	char nomFichier2Entree[255];
	char nomFichierSortie[255];
	int imgHeight;
	int imgWidth;
	int imgSize;
	
	// Prise des noms de fichier pour 
	// la lecture et l'ecriture
	sscanf (argv[1],"%s",nomFichier1Entree);
	sscanf (argv[2],"%s",nomFichier2Entree);
	sscanf (argv[3],"%s",nomFichierSortie);

	OCTET *Img1In;
	OCTET *Img2In;
	OCTET *ImgOut;

	// Prendre taille image
	lire_nb_lignes_colonnes_image_pgm(nomFichier1Entree, &imgHeight,&imgWidth);
	lire_nb_lignes_colonnes_image_pgm(nomFichier2Entree, &imgHeight,&imgWidth);
	imgSize = imgHeight * imgWidth;

	// Lire fichier donné en argument
	allocation_tableau(Img1In, OCTET, imgSize);
	allocation_tableau(Img2In, OCTET, imgSize);
	lire_image_pgm(nomFichier1Entree, Img1In, imgSize);
	lire_image_pgm(nomFichier2Entree, Img2In, imgSize);
	allocation_tableau(ImgOut, OCTET, imgSize);

	// ATTENTION : 
	// Ce programme prends en entrée une image seuillée, car sinon
	// l'algorithme donné ne marchera pas, vous donnant une image
	// entièrement blanche.

	// Pour tous les pixels de l'image :
	for (int x = 0; x < imgWidth; ++x) {
		for (int y = 0; y < imgHeight; ++y) {
			// Si les deux pixels sont différents
			if (Img1In[y*imgWidth + x] != Img2In[y*imgWidth + x]) {
				// Alors un contour :
				ImgOut[y*imgWidth + x] = 0;
			} else {
				ImgOut[y*imgWidth + x] = 255;
			}
		}
	}

	// Ecriture image
	ecrire_image_pgm(nomFichierSortie, ImgOut, imgHeight, imgWidth);

	// Liberation mémoire
	free(Img1In);
	free(Img2In);
	free(ImgOut);

	return 0;
}

void copyImage(OCTET* fileSource, OCTET* fileDest, int w, int h) {
	for (int i = 0; i < w; ++i)
		for (int j = 0; j < h; ++j)
			fileDest[i*w + j] = fileSource[i*w + j];
}
