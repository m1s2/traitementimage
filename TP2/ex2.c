#include <stdio.h>
#include "../lib/h/image_ppm.h"

#define KERNEL_SIZE_X	3
#define KERNEL_SIZE_Y	3

void copyImage(OCTET*, OCTET*, int, int);

int main(int argc, char* argv[]) {
	if (argc != 3) {
		printf("Usage : \n\terosion ImageIn.pgm ImageOut.pgm\n\n");
		exit(1);
	} else if (strcmp((const char*)"--usage", (const char*)argv[1]) == 0) {
		printf("Usage : \n\terosion ImageIn.pgm ImageOut.pgm\n\n");
		exit(1);
	}

	// Arguments nécessaires au programme
	char nomFichierEntree[255];
	char nomFichierSortie[255];
	int imgHeight;
	int imgWidth;
	int imgSize;
	
	// Prise des noms de fichier pour 
	// la lecture et l'ecriture
	sscanf (argv[1],"%s",nomFichierEntree);
	sscanf (argv[2],"%s",nomFichierSortie);

	OCTET *ImgIn;
	OCTET *ImgOut;

	// Prendre taille image
	lire_nb_lignes_colonnes_image_pgm(nomFichierEntree, &imgHeight,&imgWidth);
	imgSize = imgHeight * imgWidth;

	// Lire fichier donné en argument
	allocation_tableau(ImgIn, OCTET, imgSize);
	lire_image_pgm(nomFichierEntree, ImgIn, imgSize);
	allocation_tableau(ImgOut, OCTET, imgSize);

	// Program parameters
	int passNumber = 1;
	int isDilatation = 1;

	// ATTENTION : 
	// Ce programme prends en entrée une image seuillée, car sinon
	// l'algorithme donné ne marchera pas, vous donnant une image
	// entièrement blanche.

	// Definition de la matrice glissante pour l'effacement
	// des petits défauts :
	int** movingKernel = (int**)calloc(KERNEL_SIZE_Y, sizeof(int*));
	for (int i = 0; i < KERNEL_SIZE_Y; ++i)
		movingKernel[i] = (int*)calloc(KERNEL_SIZE_X, sizeof(int));

	if (isDilatation == 0) {
		// Full kernel :
		for (int i = 0; i < KERNEL_SIZE_Y; ++i) {
			for (int j = 0; j < KERNEL_SIZE_X; ++j) 
				movingKernel[i][j] = 255;
		}
		// Handmade kernel :
		// movingKernel[0][0] =  0 ;	movingKernel[0][1] = 255;	movingKernel[0][2] =  0 ;
		// movingKernel[1][0] = 255;	movingKernel[1][1] = 255;	movingKernel[0][2] = 255;
		// movingKernel[2][0] =  0 ;	movingKernel[2][1] = 255;	movingKernel[2][2] =  0 ;
	} else {
		// General full movingKernel :
		for (int i = 0; i < KERNEL_SIZE_Y; ++i) {
			for (int j = 0; j < KERNEL_SIZE_X; ++j) 
				movingKernel[i][j] = 0;
		}
		// Handmade kernel :
		// movingKernel[0][0] = 255;	movingKernel[0][1] =  0 ;	movingKernel[0][2] = 255;
		// movingKernel[1][0] =  0 ;	movingKernel[1][1] =  0 ;	movingKernel[0][2] =  0 ;
		// movingKernel[2][0] = 255;	movingKernel[2][1] =  0 ;	movingKernel[2][2] = 255;
	}

	int kernelCenterX = (( KERNEL_SIZE_X - 1 ) / 2);
	int kernelCenterY = (( KERNEL_SIZE_Y - 1 ) / 2);

	copyImage(ImgIn, ImgOut, imgWidth, imgHeight);

	int matchedPix = 0;

	// Pour tous les pixels de l'image :
	for (int x = 0; x < imgWidth; ++x) {
		for (int y = 0; y < imgHeight; ++y) {
			
			// On va vérifier si on peut poser le kernel
			if (movingKernel[kernelCenterY][kernelCenterX] == ImgIn[y*imgWidth + x]) {

				// On copie le kernel dans l'image de sortie
				for (int kx = 0; kx < KERNEL_SIZE_X; ++kx) {
					for (int ky = 0; ky < KERNEL_SIZE_Y; ++ky) {

						int imgOffX = kx-kernelCenterX;
						int imgOffY = ky-kernelCenterY;

						// Si on tape pas en dehors de l'image
						if (x+imgOffX >= 0 && y+imgOffY >= 0 && \
						x+imgOffX < imgWidth && y+imgOffY < imgHeight) {

							ImgOut[y*imgWidth + x + (imgOffY)*imgWidth + (imgOffX)] = movingKernel[ky][kx];

						}

					}
				}
			}
		}
	}

	// Ecriture image
	ecrire_image_pgm(nomFichierSortie, ImgOut, imgHeight, imgWidth);

	// Liberation mémoire
	free(ImgIn);
	free(ImgOut);

	return 0;
}

void copyImage(OCTET* fileSource, OCTET* fileDest, int w, int h) {
	for (int i = 0; i < w; ++i)
		for (int j = 0; j < h; ++j)
			fileDest[i*w + j] = fileSource[i*w + j];
}
