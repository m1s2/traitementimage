#include <stdio.h>
#include "../lib/h/image_ppm.h"

#define KERNEL_SIZE_X	5
#define KERNEL_SIZE_Y	5

void applyMatrixEr(OCTET*, OCTET*, int, int, int**, int, int);
void applyMatrixDil(OCTET*, OCTET*, int, int, int**, int, int);
void copyImage(OCTET*, OCTET*, int, int);
void exercerAlgorithme(int,OCTET*,OCTET*,int,int,int);

int main(int argc, char* argv[]) {
	if (argc != 3) {
		printf("Usage : \n\terosion ImageIn.pgm ImageOut.pgm\n\n");
		exit(1);
	} else if (strcmp((const char*)"--usage", (const char*)argv[1]) == 0) {
		printf("Usage : \n\terosion ImageIn.pgm ImageOut.pgm\n\n");
		exit(1);
	}
	printf("Attention, ce programme assume que vous connaissiez son utilisation. Très peu de tests\n");
	printf("sont mis en place pour éviter ce programme de ne pas fonctionner.\n");
	printf("[ Vous pouvez y acceder grace a l'option --usage ]\n");

	// Arguments nécessaires au programme
	char nomFichierEntree[255];
	char nomFichierSortie[255];
	int imgHeight;
	int imgWidth;
	int imgSize;

	// Program parameters
	int passNumber = 1;
	int isDilatation = 0;
	
	// Prise des noms de fichier pour 
	// la lecture et l'ecriture
	sscanf (argv[1],"%s",nomFichierEntree);
	sscanf (argv[2],"%s",nomFichierSortie);

	OCTET *ImgIn;
	OCTET *ImgOut;

	// Prendre taille image
	lire_nb_lignes_colonnes_image_pgm(nomFichierEntree, &imgHeight,&imgWidth);
	imgSize = imgHeight * imgWidth;

	// Lire fichier donné en argument
	allocation_tableau(ImgIn, OCTET, imgSize);
	lire_image_pgm(nomFichierEntree, ImgIn, imgSize);
	allocation_tableau(ImgOut, OCTET, imgSize);

	int oldVal, newVal;

	// ATTENTION : 
	// Ce programme prends en entrée une image seuillée, car sinon
	// l'algorithme donné ne marchera pas, vous donnant une image
	// entièrement blanche.

	exercerAlgorithme(isDilatation, ImgIn, ImgOut, imgHeight, imgWidth, passNumber);

	// Ecriture image
	ecrire_image_pgm(nomFichierSortie, ImgOut, imgHeight, imgWidth);

	// Liberation mémoire
	free(ImgIn);
	free(ImgOut);

	return 0;
}

void applyMatrixDil(OCTET* fileIn, OCTET* fileOut, int imgWidth, int imgHeight, int** kernel, int convX, int convY) {
	// Slowly slide the kernel matrix onto the image, subreptiously so no one notices

	if (convX%2 != 1 || convY%2 != 1) {
		printf("The kernel size was not setup right. Please choose another one. Exiting now ...\n");
		exit(1);
	}
	
	int xOff = (convX-1) / 2; // x offset for the kernel (also center when 0-indexed)
	int yOff = (convY-1) / 2; // y offset for the kernel (also center when 0-indexed)

	// Pour tous pixels :
	for (int i = 0; i < imgWidth; ++i) {

		for (int j = 0; j < imgHeight; j++) {

			// Number of pixels containing the kernel
			int isCovering = 0;

			// If the kernel is *in* the image, not on the side :
			if (i-xOff >= 0 && i+xOff < imgWidth && j-yOff >= 0 && j+yOff < imgHeight) {
				// For dilatation :
				// On va voir si on peut mettre le kernel :
				for (int x = 0; x < convX; ++x) {
					for (int y = 0; y < convY; ++y) {
						if (kernel[y][x] == fileIn[ i*imgWidth+j + (x-xOff)*imgWidth + (y-yOff) ] )
							isCovering++;
					}
				}
				if (isCovering != 0) {
					for (int x = 0; x < convX; ++x)
						for (int y = 0; y < convY; ++y)
							fileOut[i*imgWidth+j + (x-xOff)*imgWidth + (y-yOff)] = kernel[y][x];
				}
			} else {
				fileOut[i*imgWidth+j] = fileIn[i*imgWidth+j];
			}
			isCovering = 0;

		}

	}
}

void applyMatrixEr(OCTET* fileIn, OCTET* fileOut, int imgWidth, int imgHeight, int** kernel, int convX, int convY) {
	// Slowly slide the kernel matrix onto the image, subreptiously so no one notices

	if (convX%2 != 1 || convY%2 != 1) {
		printf("The kernel size was not setup right. Please choose another one. Exiting now ...\n");
		exit(1);
	}
	
	int xOff = (convX-1) / 2; // x offset for the kernel (also center when 0-indexed)
	int yOff = (convY-1) / 2; // y offset for the kernel (also center when 0-indexed)

	// Pour tous pixels :
	for (int i = 0; i < imgWidth; ++i) {

		for (int j = 0; j < imgHeight; j++) {

			// Number of pixels containing the kernel
			int isCovering = 0;

			// If the kernel is *in* the image, not on the side :
			if (i-xOff >= 0 && i+xOff < imgWidth && j-yOff >= 0 && j+yOff < imgHeight) {
				// For erosion :
				// On va analyser tout le kernel
				for (int x = 0; x < convX; ++x) {
					for (int y = 0; y < convY; ++y) {
						if (kernel[y][x] == fileIn[ i*imgWidth+j + (x-xOff)*imgWidth + (y-yOff) ] )
							isCovering++;
					}
				}
				if (isCovering == convX*convY) {  fileOut[i*imgWidth+j] = fileIn[i*imgWidth+j]; } 
				else {	fileOut[i*imgWidth+j] = 255; } // Forcement de l'erosion donc :
			} else {
				fileOut[i*imgWidth+j] = fileIn[i*imgWidth+j];
			}
			isCovering = 0;

		}

	}
}

void copyImage(OCTET* fileSource, OCTET* fileDest, int w, int h) {
	for (int i = 0; i < w; ++i)
		for (int j = 0; j < h; ++j)
			fileDest[i*w + j] = fileSource[i*w + j];
}

void exercerAlgorithme(int isDilatation, OCTET* ImgIn, OCTET* ImgOut, int imgHeight, int imgWidth, int passNumber) {
	// Definition de la matrice glissante pour l'effacement
	// des petits défauts :
	int** movingKernel = (int**)calloc(KERNEL_SIZE_Y, sizeof(int*));
	for (int i = 0; i < KERNEL_SIZE_Y; ++i)
		movingKernel[i] = (int*)calloc(KERNEL_SIZE_X, sizeof(int));

	if (isDilatation == 1) {
		// Full kernel :
		for (int i = 0; i < KERNEL_SIZE_Y; ++i) {
			for (int j = 0; j < KERNEL_SIZE_X; ++j) 
				movingKernel[i][j] = 255;
		}
		// Handmade kernel :
		// movingKernel[0][0] =  0 ; movingKernel[0][1] = 255; movingKernel[0][2] =  0 ;

		// movingKernel[1][0] = 255; movingKernel[1][1] = 255;	movingKernel[0][2] = 255;
		
		// movingKernel[2][0] =  0 ; movingKernel[2][1] = 255;	movingKernel[2][2] =  0 ;
	} else {
		// General full movingKernel :
		for (int i = 0; i < KERNEL_SIZE_Y; ++i) {
			for (int j = 0; j < KERNEL_SIZE_X; ++j) 
				movingKernel[i][j] = 0;
		}
		// Handmade kernel :
		// movingKernel[0][0] = 255; movingKernel[0][1] =  0 ; movingKernel[0][2] = 255;

		// movingKernel[1][0] =  0 ; movingKernel[1][1] =  0 ;	movingKernel[0][2] =  0 ;
		
		// movingKernel[2][0] = 255; movingKernel[2][1] =  0 ;	movingKernel[2][2] = 255;
	}

	void (*func)() = (isDilatation == 1) ? &applyMatrixDil : &applyMatrixEr;

	func(ImgIn, ImgOut, imgWidth, imgHeight, movingKernel, KERNEL_SIZE_X, KERNEL_SIZE_Y);
	for (int i = 1; i < passNumber; ++i) {
		copyImage(ImgOut, ImgIn, imgWidth, imgHeight);
		func(ImgIn, ImgOut, imgWidth, imgHeight, movingKernel, KERNEL_SIZE_X, KERNEL_SIZE_Y);
	}
}
