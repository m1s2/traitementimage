#include <stdio.h>
#include <math.h>
#include <fcntl.h>
#include "../lib/h/image_ppm.h"
#include "../lib/h/tp.h"

enum typeDeRepliement {
	BOTTOM,TOP
};

enum colorOffsets {
	RED,
	GREEN,
	BLUE
};

// Permet de ne pas se répéter trop dans les fonctions
// de manipulation d'histogramme.
#define PARCOURSHISTO (int i = 0; i < 256; ++i) 

int getTransformedVal(int,int,int);		// Transformation lineaire

double* getSpecialHisto(OCTET*, int, int);
double getInverseValue(double*, int);

void transformImage(OCTET*, int, OCTET*);

int main(int argc, char* argv[]) {
	if (argc != 4) {
		printf("Usage : \n\tspecialiseHisto ImageRef.pgm ImageIn.pgm ImageOut.pgm\n\n");
		exit(1);
	} else if (strcmp((const char*)"--usage", (const char*)argv[1]) == 0) {
		printf("Usage : \n\texpansionHisto ImageRef.pgm ImageIn.pgm ImageOut.pgm\n\n");
		exit(1);
	}

	// Arguments nécessaires au programme
	char nomFichierRefer[255];
	char nomFichierEntree[255];
	char nomFichierSortie[255];
	int imgHeightRef;
	int imgHeight;
	int imgWidthRef;
	int imgWidth;
	int imgSizeRef;
	int imgSize;

	// Prise des noms de fichier pour 
	// la lecture et l'ecriture
	sscanf (argv[1],"%s",nomFichierRefer);
	sscanf (argv[2],"%s",nomFichierEntree);
	sscanf (argv[3],"%s",nomFichierSortie);

	OCTET *ImgIn;
	OCTET *ImgRef;
	OCTET *ImgOut;

	// Prendre taille image
	lire_nb_lignes_colonnes_image_pgm(nomFichierRefer, &imgHeightRef,&imgWidthRef);
	imgSizeRef = imgHeightRef * imgWidthRef;
	lire_nb_lignes_colonnes_image_pgm(nomFichierEntree, &imgHeight,&imgWidth);
	imgSize = imgHeight * imgWidth;

	// Lire fichiers donné en argument
	allocation_tableau(ImgRef, OCTET, imgSizeRef);
	lire_image_pgm(nomFichierRefer, ImgRef, imgSizeRef);

	allocation_tableau(ImgIn, OCTET, imgSize);
	lire_image_pgm(nomFichierEntree, ImgIn, imgSize);

	allocation_tableau(ImgOut, OCTET, imgSize);

	double* inverseTransform = getSpecialHisto(ImgRef, imgWidthRef, imgHeightRef);

	// Transformation originelle image :
	transformImage(ImgIn, imgSize, ImgOut);

	// Transformation image :
	for (int i = 0; i < imgSize; i+=1) {
		ImgOut[i] = (OCTET)floor(inverseTransform[ImgIn[i]]);
	}

	// On le recalcule pour l'exploiter après
	int* histoValues = histogramme(ImgOut, imgWidth, imgHeight, "plot/ex3/newExtrema.dat");

	ecrire_image_pgm(nomFichierSortie, ImgOut, imgHeight, imgWidth);

	// free(histoValues);
	free(ImgIn);
	free(ImgOut);

	return 0;
}

int getTransformedVal(int p_i, int amin, int amax) {
	// Obtenue
	double coef = ( (double)255 / (double)(amax-amin) );
	return (int) round( coef * (p_i - amin ) );
}

double* getSpecialHisto(OCTET* image, int imgWidth, int imgHeight) {
	// Variables et tableaux utilisé(e)s
	int imgSize = imgHeight * imgWidth;
	int* histo = calloc(256, sizeof(double));
	double* ddp = calloc(256, sizeof(double));
	double* cumul = calloc(256, sizeof(double));
	double* transformation = calloc(256, sizeof(double));
	double* inverse = calloc(256,sizeof(double));
	FILE* probaFile;

	// Creer histogramme originel :
	for (int i = 0; i < imgSize; ++i) { histo[image[i]]++; }
	probaFile = fopen((const char*)"plot/ex4/histoRef.dat", "w");
	for PARCOURSHISTO { fprintf(probaFile, "%i\n", histo[i]); }
	fclose(probaFile);
	// Faire DDP(x) :
	for PARCOURSHISTO { ddp[i] = ((double) histo[i]) / ((double)imgSize); }
	// Faire la rampe :
	double last = 0;
	for PARCOURSHISTO { cumul[i] = ddp[i] + last; last = cumul[i]; }
	// Faire la fonction de transformation directe :
	for PARCOURSHISTO { transformation[i] = (double)255.0 * cumul[i]; }
	
	probaFile = fopen((const char*)"plot/ex4/stage4.dat", "w");
	for PARCOURSHISTO { fprintf(probaFile, "%f\n", transformation[i]); }
	fclose(probaFile);
	
	// Prendre la transformation inverse :
	for PARCOURSHISTO { inverse[i] = getInverseValue(transformation,i); }
	
	// Ecrit tout ca dans un fichier :
	probaFile = fopen((const char*)"plot/ex4/stage5.dat", "w");
	for (int i = 1; i < 256; ++i) {
		fprintf(probaFile, "%f\n", inverse[i]);
	}
	fclose(probaFile);

	free(histo);
	free(ddp);
	free(cumul);
	free(transformation);

	return inverse;
}

double getInverseValue(double* transf, int needed) {
	int k = 0;
	while (k < 255) {
		if ( floor(transf[k]) == (double)needed )
		{	break;		}
		if ( floor(transf[k]) > (double)needed )
		{	break;		}
		k++;
	}
	if (k == 255) {
		printf("For index %i, k was not found.\n", needed);
	}
	return (double)k;
}

void transformImage(OCTET* in, int size, OCTET* out) {
	// Variables et tableaux utilisé(e)s
	int* histo = calloc(256, sizeof(int));
	double* ddp = calloc(256, sizeof(double));
	double* cumul = calloc(256, sizeof(double));
	double* transformation = calloc(256, sizeof(double));
	double* inverse = calloc(256,sizeof(double));
	FILE* probaFile;

	// Creer histogramme originel :
	for (int i = 0; i < size; ++i) { histo[in[i]]++; }
	probaFile = fopen((const char*)"plot/ex4/histoOrig.dat", "w");
	for PARCOURSHISTO { fprintf(probaFile, "%i\n", histo[i]); }
	fclose(probaFile);
	// Faire DDP(x) :
	for PARCOURSHISTO { ddp[i] = ((double) histo[i]) / ((double)size); }
	// Faire la rampe :
	double last = 0;
	for PARCOURSHISTO { cumul[i] = ddp[i] + last; last = cumul[i]; }
	// Faire la fonction de transformation directe :
	for PARCOURSHISTO { transformation[i] = (double)255.0 * cumul[i]; }

	for (int i = 0; i < size; ++i) {
		out[i] = (OCTET)round(transformation[in[i]]);
	}

	free(histo);
	histo = calloc(256, sizeof(int));

	for (int i = 0; i < size; ++i) { histo[out[i]]++; }
	probaFile = fopen((const char*)"plot/ex4/histoMod.dat", "w");
	for PARCOURSHISTO { fprintf(probaFile, "%i\n", histo[i]); }
	fclose(probaFile);

	free(histo);
	free(ddp);
	free(cumul);
	free(transformation);
	return;
}
