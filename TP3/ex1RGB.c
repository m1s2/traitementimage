#include <stdio.h>
#include <math.h>
#include <fcntl.h>
#include "../lib/h/image_ppm.h"
#include "../lib/h/tp.h"

int getTransformedVal(int,int,int);

int main(int argc, char* argv[]) {
	if (argc != 3) {
		printf("Usage : \n\texpansionHisto ImageIn.pgm ImageOut.pgm\n\n");
		exit(1);
	} else if (strcmp((const char*)"--usage", (const char*)argv[1]) == 0) {
		printf("Usage : \n\texpansionHisto ImageIn.pgm ImageOut.pgm\n\n");
		exit(1);
	}

	// Arguments nécessaires au programme
	char nomFichierEntree[255];
	char nomFichierSortie[255];
	int imgHeight;
	int imgWidth;
	int imgSize;
	
	// Prise des noms de fichier pour 
	// la lecture et l'ecriture
	sscanf (argv[1],"%s",nomFichierEntree);
	sscanf (argv[2],"%s",nomFichierSortie);

	OCTET *ImgIn;
	OCTET *ImgOut;

	// Prendre taille image
	lire_nb_lignes_colonnes_image_ppm(nomFichierEntree, &imgHeight,&imgWidth);
	imgSize = imgHeight * imgWidth;

	// Lire fichier donné en argument
	allocation_tableau(ImgIn, OCTET, imgSize*3);
	lire_image_ppm(nomFichierEntree, ImgIn, imgSize);
	allocation_tableau(ImgOut, OCTET, imgSize*3);

	int** histoValues = histogrammeRGB(ImgIn, imgWidth, imgHeight, "plot/ex1/histoRGB.dat");

	for (int i = 0; i < imgSize*3; i+=3) {
		ImgOut[i+0] = getTransformedVal(ImgIn[i+0], 0, 12);
		ImgOut[i+1] = getTransformedVal(ImgIn[i+1], 0, 11);
		ImgOut[i+2] = getTransformedVal(ImgIn[i+2], 0, 9);
	}

	histoValues = histogrammeRGB(ImgOut, imgWidth, imgHeight, "plot/ex1/expandedHistoRGB.dat");

	ecrire_image_ppm(nomFichierSortie, ImgOut, imgHeight, imgWidth);
	
	free(histoValues);
	free(ImgIn);
	free(ImgOut);
	
	return 0;
}

int getTransformedVal(int p_i, int amin, int amax) {
	// Obtenue
	double coef = ( (double)255 / (double)(amax-amin) );
	return (int) round( coef * (p_i - amin ) );
}