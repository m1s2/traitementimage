#include <stdio.h>
#include <math.h>
#include <fcntl.h>
#include "../lib/h/image_ppm.h"
#include "../lib/h/tp.h"

enum typeDeRepliement {
	BOTTOM,TOP
};

enum colorOffsets {
	RED,
	GREEN,
	BLUE
};

int getTransformedVal(int,int,int);		// Transformation lineaire
void copyImage(OCTET*, OCTET*, int, int);
double* probabilityDensity(int*);
double* valuedDensity(double*);
double* cumulativeDensity(double*);

int main(int argc, char* argv[]) {
	if (argc != 3) {
		printf("Usage : \n\texpansionHisto ImageIn.pgm ImageOut.pgm\n\n");
		exit(1);
	} else if (strcmp((const char*)"--usage", (const char*)argv[1]) == 0) {
		printf("Usage : \n\texpansionHisto ImageIn.pgm ImageOut.pgm\n\n");
		exit(1);
	}

	// Arguments nécessaires au programme
	char nomFichierEntree[255];
	char nomFichierSortie[255];
	int imgHeight;
	int imgWidth;
	int imgSize;

	// Prise des noms de fichier pour 
	// la lecture et l'ecriture
	sscanf (argv[1],"%s",nomFichierEntree);
	sscanf (argv[2],"%s",nomFichierSortie);

	OCTET *ImgIn;
	OCTET *ImgOut;

	// Prendre taille image
	lire_nb_lignes_colonnes_image_pgm(nomFichierEntree, &imgHeight,&imgWidth);
	imgSize = imgHeight * imgWidth;

	// Lire fichier donné en argument
	allocation_tableau(ImgIn, OCTET, imgSize);
	lire_image_pgm(nomFichierEntree, ImgIn, imgSize);
	allocation_tableau(ImgOut, OCTET, imgSize);

	int* histoValues = histogramme(ImgIn, imgWidth, imgHeight, "plot/ex3/oldHisto.dat");

	double* densities = probabilityDensity(histoValues);
	// Calcul de la "rampe" de l'histogramme
	double* f_a = cumulativeDensity(densities);
	// Calcul de la rampe 
	double* t_a = valuedDensity(f_a);
	
	// Transformation image :
	for (int i = 0; i < imgSize; i+=1) {
		ImgOut[i] = (OCTET)round(t_a[ImgIn[i]]);
	}

	free(histoValues);

	// On le recalcule pour l'exploiter après
	histoValues = histogramme(ImgOut, imgWidth, imgHeight, "plot/ex3/newExtrema.dat");

	ecrire_image_pgm(nomFichierSortie, ImgOut, imgHeight, imgWidth);
	
	// free(histoValues);
	free(ImgIn);
	free(ImgOut);
	
	return 0;
}

int getTransformedVal(int p_i, int amin, int amax) {
	// Obtenue
	double coef = ( (double)255 / (double)(amax-amin) );
	return (int) round( coef * (p_i - amin ) );
}

void copyImage(OCTET* fileSource, OCTET* fileDest, int w, int h) {
	for (int i = 0; i < h; ++i){
		for (int j = 0; j < w; j +=1) {
			fileDest[i*w + j] = fileSource[i*w + j];
		}
	}
}

double* probabilityDensity(int* histo) {
	double* densities = calloc(256, sizeof(double));
	// Calcule le nb de valeurs
	double nbValues = 0;
	for (int i = 0; i < 256; ++i) { nbValues += histo[i]; }
	printf("Total values : %f\n", nbValues);
	FILE* probaFile = fopen((const char*)"plot/ex3/probaDensity.dat", "w");
	// Calcule la densité de probabilité
	for (int i = 0; i < 256; ++i) {
		densities[i] = ( ((double) histo[i]) / nbValues );
		// printf("Calculating : %i / %f = %f\n", histo[i], nbValues, densities[i]);
		fprintf(probaFile, "%f\n", densities[i]);
	}
	fclose(probaFile);
	return densities;
}

double* valuedDensity(double* probas) {
	double* values = calloc(256,sizeof(double));
	FILE* probaFile = fopen((const char*)"plot/ex3/valuedDensity.dat", "w");
	for (int i = 0; i < 256; ++i) {
		values[i] = 255.0 * probas[i];
		fprintf(probaFile, "%f\n", values[i]);
	}
	fclose(probaFile);
	return values;
}

double* cumulativeDensity(double* values) {
	double* ramp = calloc(256, sizeof(double));
	FILE* probaFile = fopen((const char*)"plot/ex3/cumulatedDensity.dat", "w");
	ramp[0] = values[0];
	fprintf(probaFile, "%f\n", ramp[0]);
	for (int i = 1; i < 256; ++i) {
		ramp[i] = values[i] + ramp[i-1];
		fprintf(probaFile, "%f\n", ramp[i]);
	}
	fclose(probaFile);
	return ramp;
}
