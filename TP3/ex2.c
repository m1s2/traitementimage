#include <stdio.h>
#include <math.h>
#include <fcntl.h>
#include "../lib/h/image_ppm.h"
#include "../lib/h/tp.h"

enum typeDeRepliement {
	BOTTOM,TOP
};

enum colorOffsets {
	RED	= 0,
	GREEN	= 1,
	BLUE	= 2
};

int inferieurA(int,int);			// a <= b ?
int superieurA(int,int);			// a >= b ?
int getTransformedVal(int,int,int);		// Transformation lineaire
void copyImage(OCTET*, OCTET*, int, int);
void replierHisto(OCTET*, int, int, int, int, int);

int main(int argc, char* argv[]) {
	if (argc != 3) {
		printf("Usage : \n\texpansionHisto ImageIn.pgm ImageOut.pgm\n\n");
		exit(1);
	} else if (strcmp((const char*)"--usage", (const char*)argv[1]) == 0) {
		printf("Usage : \n\texpansionHisto ImageIn.pgm ImageOut.pgm\n\n");
		exit(1);
	}

	// Arguments nécessaires au programme
	char nomFichierEntree[255];
	char nomFichierSortie[255];
	int imgHeight;
	int imgWidth;
	int imgSize;

	// Prise des noms de fichier pour 
	// la lecture et l'ecriture
	sscanf (argv[1],"%s",nomFichierEntree);
	sscanf (argv[2],"%s",nomFichierSortie);

	OCTET *ImgIn;
	OCTET *ImgOut;

	// Prendre taille image
	lire_nb_lignes_colonnes_image_ppm(nomFichierEntree, &imgHeight,&imgWidth);
	imgSize = imgHeight * imgWidth;

	// Lire fichier donné en argument
	allocation_tableau(ImgIn, OCTET, imgSize*3);
	lire_image_ppm(nomFichierEntree, ImgIn, imgSize);
	allocation_tableau(ImgOut, OCTET, imgSize*3);

	int** histoValues = histogrammeRGB(ImgIn, imgWidth, imgHeight, "plot/ex2/oldHisto.dat");

	// Valeurs pour : 
	//	peppers.ppm
	int minR =  50; int maxR = 250;
	int minG =  50; int maxG = 220;
	int minB =   0; int maxB = 200;

	// Segmenter l'histogramme rouge :
	replierHisto(ImgIn, imgWidth, imgHeight, BOTTOM,   RED, minR);
	replierHisto(ImgIn, imgWidth, imgHeight,    TOP,   RED, maxR);
	// Segmenter l'histogramme vert :
	replierHisto(ImgIn, imgWidth, imgHeight, BOTTOM, GREEN, minG);
	replierHisto(ImgIn, imgWidth, imgHeight,    TOP, GREEN, maxG);
	// Segmenter l'histogramme bleu :
	replierHisto(ImgIn, imgWidth, imgHeight, BOTTOM,  BLUE, minB);
	replierHisto(ImgIn, imgWidth, imgHeight,    TOP,  BLUE, maxB);

	ecrire_image_ppm("out/tempo.ppm", ImgIn, imgHeight, imgWidth);

	// On le recalcule pour l'exploiter après
	histoValues = histogrammeRGB(ImgIn, imgWidth, imgHeight, "plot/ex2/newHisto.dat");

	// Expansion des histogrammes obtenus :
	for (int i = 0; i < imgSize*3; i+=3) {
		ImgOut[i+0] = getTransformedVal(ImgIn[i+0], minR, maxR);
		ImgOut[i+1] = getTransformedVal(ImgIn[i+1], minG, maxG);
		ImgOut[i+2] = getTransformedVal(ImgIn[i+2], minB, maxB);
	}

	free(histoValues);

	// On le recalcule pour l'exploiter après
	histoValues = histogrammeRGB(ImgOut, imgWidth, imgHeight, "plot/ex2/newExtrema.dat");

	ecrire_image_ppm(nomFichierSortie, ImgOut, imgHeight, imgWidth);
	
	free(histoValues);
	free(ImgIn);
	free(ImgOut);
	
	return 0;
}

int getTransformedVal(int p_i, int amin, int amax) {
	// Obtenue
	double coef = ( (double)255 / (double)(amax-amin) );
	return (int) round( coef * (p_i - amin ) );
}

void replierHisto(OCTET* Img, int imgWidth, int imgHeight, int type, int colorOffset, int threshold) {
	// Fonction de comparaison de valeurs
	int (*comparaison)(int,int) = (type == BOTTOM) ? &inferieurA : &superieurA;
	// Number of values (nbPixels * 3 channels) :
	int i3 = imgWidth * imgHeight * 3;

	for (int i = 0; i < i3; i += 3) {
		if ((*comparaison)(threshold, Img[i+colorOffset]) == 1) {
			Img[i+colorOffset] = threshold;
		}
	}
	
	return;
}

int inferieurA(int threshold, int value) {
	return (value < threshold) ? 1 : 0;
}

int superieurA(int threshold, int value) {
	return (value > threshold) ? 1 : 0;
}

void copyImage(OCTET* fileSource, OCTET* fileDest, int w, int h) {
	for (int i = 0; i < h; ++i){
		for (int j = 0; j < w*3; j += 3) {
			fileDest[i*w*3 + j+0] = fileSource[i*w*3 + j+0];
			fileDest[i*w*3 + j+1] = fileSource[i*w*3 + j+1];
			fileDest[i*w*3 + j+2] = fileSource[i*w*3 + j+2];
		}
	}
}
