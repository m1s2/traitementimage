#include <stdio.h>
#include <math.h>
#include <fcntl.h>
#include "../lib/h/image_ppm.h"
#include "../lib/h/tp.h"

int getTransformedVal(int);

int main(int argc, char* argv[]) {
	if (argc != 3) {
		printf("Usage : \n\texpansionHisto ImageIn.pgm ImageOut.pgm\n\n");
		exit(1);
	} else if (strcmp((const char*)"--usage", (const char*)argv[1]) == 0) {
		printf("Usage : \n\texpansionHisto ImageIn.pgm ImageOut.pgm\n\n");
		exit(1);
	}

	// Arguments nécessaires au programme
	char nomFichierEntree[255];
	char nomFichierSortie[255];
	int imgHeight;
	int imgWidth;
	int imgSize;
	
	// Prise des noms de fichier pour 
	// la lecture et l'ecriture
	sscanf (argv[1],"%s",nomFichierEntree);
	sscanf (argv[2],"%s",nomFichierSortie);

	OCTET *ImgIn;
	OCTET *ImgOut;

	// Prendre taille image
	lire_nb_lignes_colonnes_image_pgm(nomFichierEntree, &imgHeight,&imgWidth);
	imgSize = imgHeight * imgWidth;

	// Lire fichier donné en argument
	allocation_tableau(ImgIn, OCTET, imgSize);
	lire_image_pgm(nomFichierEntree, ImgIn, imgSize);
	allocation_tableau(ImgOut, OCTET, imgSize);

	int* histoValues = histogramme(ImgIn, imgWidth, imgHeight, "plot/ex1/histo.dat");

	// Obtenues empiriquement
	int amin = 0;
	int amax = 10;

	for (int i = 0; i < imgSize; ++i) {
		ImgOut[i] = getTransformedVal(ImgIn[i]);
	}

	histoValues = histogramme(ImgOut, imgWidth, imgHeight, "plot/ex1/expandedHisto.dat");

	ecrire_image_pgm(nomFichierSortie, ImgOut, imgHeight, imgWidth);
	
	free(histoValues);
	free(ImgIn);
	// free(ImgOut);
	
	return 0;
}

int getTransformedVal(int p_i) {
	int amax = 10;
	int amin = 0;
	double coef = ( (double)255 / (double)(amax-amin) );
	return (int) round( coef * (p_i - amin ) );
}